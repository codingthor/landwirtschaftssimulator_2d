package org.items;

import org.Player;
import org.World;
import org.ground.Ground;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;

class PlowTest {
    private int gridType;

    @BeforeEach
    void setUp() {
        World world = new World(20, 20);
        world.fillWorld(world.getDefaultGroundMap());
        Player player = new Player();
        player.setX(100);
        player.setY(100);
        int playerX = player.calculateCoordinates(player.getX());
        int playerY = player.calculateCoordinates(player.getY());
        gridType = world.getGridType(playerX, playerY);
}

    @org.junit.jupiter.api.Test
    void use() {
        assertEquals(1, gridType);
        assertNotEquals(2, gridType);
    }

}