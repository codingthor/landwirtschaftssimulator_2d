package org.vehicles;

import org.Player;
import org.World;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VehicleManagerTest {
    private VehicleClass[] vehicleCollection;
    private final Player player = new Player();
    private final VehicleManager manager = new VehicleManager();

    @BeforeEach
    void setUp(){
        World world = new World(20, 20);
        vehicleCollection = manager.getVehicleCollection();
        VehicleClass vC = this.vehicleCollection[0];
        vC.setX(100);
        vC.setY(100);
        player.setX(100);
        player.setY(100);
    }
    @Test
    void sattleOn() {
        manager.sattleOn(player);
        assertTrue(player.getOnVehicle());
        assertEquals("Tractor", manager.getActiveVehicle().getImg());
        assertNotEquals("Bus", manager.getActiveVehicle().getImg());
    }
}