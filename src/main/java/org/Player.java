package org;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.items.*;

public class Player extends Entity {

    private static final Logger log = LogManager.getLogger(Player.class);

    private final double defaultSpeed = 2.0;
    private double speed;
    private boolean hasHarvester = false;
    private boolean hasTractor = false;

    private boolean hasATV = true;
    private boolean onVehicle = false;
    private final Item currency;
    private final Item hand;
    private final Item plow;
    private final Item wheat;
    private final Item potato;

    private final Item[] inv = new Item[4];



    public Player(){

        currency = new Currency(0);
        hand = new Hand(1);
        plow = new Plow(1);
        wheat = new Wheat(0);
        potato = new Potato(0);

        inv[0] = hand;
        inv[1] = plow;
        inv[2] = wheat;
        inv[3] = potato;
        inv[0].setActive(true);
        speed = 2;
        setImg("Player");
    }

    public void setSpeed(double speed) {
        this.speed = speed;
        log.debug("Speed set to: " + this.speed);
    }

    public double getSpeed() {
        log.debug("Speed returned: " + this.speed);
        return speed;
    }

    public void setHasHarvester(boolean hasPlow) {
        this.hasHarvester = hasPlow;
        log.info("Plow set to: " + this.hasHarvester);
    }

    public boolean getHasHarvester() {
        log.info("Status of harvester returned:  " + this.hasHarvester);
        return this.hasHarvester;
    }

    public boolean getHasATV() {
        return hasATV;
    }

    public void setHasATV(boolean hasATV) {
        log.info("Status of ATV returned:  " + this.hasATV);
        this.hasATV = hasATV;
    }

    public double getDefaultSpeed() {
        return defaultSpeed;
    }

    public void setHasTractor(boolean hasTractor) {
        this.hasTractor = hasTractor;
        log.info("Tractor set to: " + this.hasTractor);
    }

    public boolean getHasTractor() {
        log.info("Status of tractor returned:  " + this.hasTractor);
        return this.hasTractor;
    }


    public Item getCurrencyItem(){
        log.debug("used getCurrencyItem()");
        return currency;
    }
    public Item getWheatItem(){
        log.debug("used getWheatItem()");
        return wheat;
    }
    public Item getPotatoItem(){
        log.debug("used getPotatoItem()");
        return potato;
    }
    public Item getHandItem(){
        log.debug("used getHandItem()");
        return hand;
    }
    public Item getPlowItem(){
        log.debug("used getPlowItem()");
        return plow;
    }

    public void setOnVehicle(boolean b){
        onVehicle = b;
        log.debug("Player sat on vehicle");
    }

    public boolean getOnVehicle(){
        log.debug("Is on Vehicle: " + onVehicle);
        return onVehicle;
    }

    public Item getInvItem(int i){
        return inv[i];
    }

    public void invSetActive(int activate){
        log.debug("used invSetActive");
        for (Item item : inv) {
            item.setActive(false);
        }
        inv[activate].setActive(true);
        log.debug("Item inv[" + activate + "] is now active");
    }

    public Item invGetActive(){
        Item activeItem = null;
        for (Item item : inv){
            if (item.isActive()){
                activeItem = item;
            }
        }
        return activeItem;
    }

}
