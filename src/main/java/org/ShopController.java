package org;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import org.vehicles.VehicleFactory;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class ShopController {


    Shop shop;
    Player player;
    Label currencyAmountLabel;
    Label shopTitle;
    Label wheatAmountLabel;
    Label potatoAmountLabel;
    Label harvesterAmountLabel;
    Label tractorAmountLabel;
    Label atvAmountLabel;
    AtomicInteger wheatCounter = new AtomicInteger();
    AtomicInteger potatoCounter = new AtomicInteger();
    AtomicInteger tractorCounter = new AtomicInteger();
    AtomicInteger harvesterCounter = new AtomicInteger();
    AtomicInteger atvCounter = new AtomicInteger();
    Button sellWheatButton = new Button(" -\n12€");
    Button buyWheatButton = new Button(" +\n15€");
    Button sellPotatoButton = new Button(" -\n16€");
    Button buyPotatoButton = new Button(" +\n19€");
    Button sellHarvesterButton = new Button(" -\n--€");
    Button buyHarvesterButton = new Button(" +\n500€");
    Button sellTractorButton = new Button(" -\n--€");
    Button buyTractorButton = new Button(" +\n600€");
    Button sellATVButton = new Button("-");
    Button buyATVButton = new Button("+");
    Button cShop = new Button("Crop-Shop");
    Button vShop = new Button("Vehicle-Shop");
    Button sellAllButton = new Button("Sell All!");
    Button sellSelectedButton = new Button("Check Out!");
    Button sellSelectedButtonvShop = new Button("Check Out!");
    VehicleFactory vehicleFactory;


    ShopController(HashMap<String, Image> imageMap,Player p, Shop shop, VehicleFactory vehicleFactory){
        this.shop = shop;

        this.player = p;
        this.vehicleFactory = vehicleFactory;

        this.currencyAmountLabel = new Label("" + p.getCurrencyItem().getAmount());
        this.currencyAmountLabel.setStyle("-fx-font-size: 30");

        this.shopTitle = new Label("Farm-Shop");
        this.shopTitle.setStyle("-fx-font-size: 50");
        this.shopTitle.setAlignment(Pos.CENTER_LEFT);

        this.wheatAmountLabel = new Label("" + this.wheatCounter);
        this.wheatAmountLabel.setStyle("-fx-font-size: 30");

        this.potatoAmountLabel = new Label("" + this.potatoCounter);
        this.potatoAmountLabel.setStyle("-fx-font-size: 30");

        this.harvesterAmountLabel = new Label("" + this.harvesterCounter);
        this.harvesterAmountLabel.setStyle("-fx-font-size: 30");

        this.tractorAmountLabel = new Label("" + this.tractorCounter);
        this.tractorAmountLabel.setStyle("-fx-font-size: 30");

        this.atvAmountLabel = new Label("" + this.atvCounter);
        this.atvAmountLabel.setStyle("-fx-font-size: 30");

    }

    public void sellWheatButtonListener(){
        wheatCounter.set(wheatCounter.get()-1);
        wheatAmountLabel.setText("" + wheatCounter);
    }

    public void buyWheatButtonListener(){
        wheatCounter.set(wheatCounter.get()+1);
        wheatAmountLabel.setText("" + wheatCounter);
    }

    public void sellPotatoButtonListener(){
        potatoCounter.set(potatoCounter.get()-1);
        potatoAmountLabel.setText("" + potatoCounter);
    }

    public void buyPotatoButtonListener(){
        potatoCounter.set(potatoCounter.get()+1);
        potatoAmountLabel.setText("" + potatoCounter);
    }

    public void sellPlowButtonListener(){
        harvesterCounter.set(0);
        harvesterAmountLabel.setText("" + harvesterCounter);
    }

    public void buyPlowButtonListener(){
        harvesterCounter.set(1);
        harvesterAmountLabel.setText("" + harvesterCounter);
    }

    public void sellTractorButtonListener(){
        tractorCounter.set(0);
        tractorAmountLabel.setText("" + tractorCounter);
    }

    public void buyTractorButtonListener(){
        tractorCounter.set(1);
        tractorAmountLabel.setText("" + tractorCounter);
    }

    public void cShopButtonListener(){
        this.harvesterCounter.set(0);
        this.tractorCounter.set(0);
    }

    public void vShopButtonListener(){
        this.wheatCounter.set(0);
        this.potatoCounter.set(0);
    }

    public void sellAllButtonListener(){
        shop.sellAll();
        currencyAmountLabel.setText("" + player.getCurrencyItem().getAmount());
        wheatAmountLabel.setText("" + player.getWheatItem().getAmount());
        potatoAmountLabel.setText("" + player.getPotatoItem().getAmount());
    }

    public void setSellSelectedButtonListener(){
        shop.shopSystem(this.wheatCounter.get(), this.potatoCounter.get(), this.harvesterCounter.get(), this.tractorCounter.get());
        this.wheatCounter.set(0);
        this.potatoCounter.set(0);
        this.harvesterCounter.set(0);
        this.tractorCounter.set(0);
        this.wheatAmountLabel.setText("" + this.wheatCounter.get());
        this.potatoAmountLabel.setText("" + this.potatoCounter.get());
        this.harvesterAmountLabel.setText("" + this.harvesterCounter.get());
        this.tractorAmountLabel.setText("" + this.tractorCounter.get());
        this.currencyAmountLabel.setText("" + player.getCurrencyItem().getAmount());
    }

    public void setSellSelectedButtonvShopListener() {
        log.info("called setSellSelectedButtonvShopListener()");
        log.info(this.harvesterCounter.get()==1);
        log.info(player.getCurrencyItem().getAmount()>500);

        if (this.harvesterCounter.get()==1&&player.getCurrencyItem().getAmount()>500){
            log.info("buy harvester");
            player.getCurrencyItem().setAmount(player.getCurrencyItem().getAmount()-500);
            this.harvesterCounter.set(0);
            player.setHasHarvester(true);
            vehicleFactory.createVehicle("Harvester", shop.getX(), shop.getY());
        }
        if (this.tractorCounter.get()==1&&player.getCurrencyItem().getAmount()>500){
            log.info("buy tractor");
            player.getCurrencyItem().setAmount(player.getCurrencyItem().getAmount()-600);
            this.tractorCounter.set(0);
            player.setHasTractor(true);
            vehicleFactory.createVehicle("Tractor", shop.getX(), shop.getY());
        }
    }

    private static final Logger log = LogManager.getLogger(ShopController.class);



}
