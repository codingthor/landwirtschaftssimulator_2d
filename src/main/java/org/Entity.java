package org;


public abstract class Entity {
    private double x;

    private double y;

    private String img;


    public int getX(){
        return (int)this.x;
    }

    public int getY(){
        return (int)this.y;
    }

    public double getXAsDouble(){
        return this.x;
    }

    public double getYAsDouble(){
        return this.y;
    }


    public void setX(double a){
        this.x = a;
    }

    public void setY(double b) {
        this.y = b;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImg() {
        return img;
    }

    public int calculateCoordinates(int c){
        return (c + 25) / 50;

    }
}
