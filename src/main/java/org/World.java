package org;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ground.*;
import org.ground.crops.PlantedGround;
import org.ground.crops.PlantedGroundReady;
import org.items.Item;


public class World {
    private static final Logger log = LogManager.getLogger(World.class);
    private final int gridSizeX;
    private final int gridSizeY;
    private final Ground[][] grid;
    private final Ground[][] gridPlanted;
    private final int[][] defaultGroundMap;
    private final int[][] defaultPlantedMap;






    public World(int x, int y) {
        this.gridSizeX = x;
        this.gridSizeY = y;
        grid = new Ground[x][y];
        gridPlanted = new Ground[x][y];
        defaultGroundMap = new int[x][y];
        defaultPlantedMap = new int[x][y];


        /*
        #####COMMENT########
        the default map array is filled with,
        Later in the fillWorld Method this groundMap is interpreted as Ground classes.
        Every Ground has a index wich is equivalent to the int in this map
         */
        for(int i = 0; i<defaultGroundMap.length; i++){
            for(int j = 0; j<defaultGroundMap.length; j++) {
                defaultGroundMap[i][j] = 1;
                defaultPlantedMap[i][j] = 404;

            }
        }


    }

    public int getGridSizeX() {
        return gridSizeX;
    }

    public int getGridSizeY() {
        return gridSizeY;
    }

    public int getGridType(int x, int y) {
        return grid[x][y].getIndex();
    }

    public int getGridPlantedType(int x, int y) {
            return gridPlanted[x][y].getIndex();
        }


    public Ground[][] getGrid() {
        return this.grid;
    }
    public Ground[][] getGridPlanted() {
        return this.gridPlanted;
    }

    public void changeToField(int x, int y){
        grid[x][y] = new Field();
    }

    public void changeToPlantedGround(int x, int y, Item item){
        log.info("changed to PlantedGround");
        gridPlanted[x][y] = new PlantedGround(item);
    }
    public void changeToPlantedGroundReady(int x, int y, Item item){
        log.info("changed to PlantedGroundReady");
        gridPlanted[x][y] = new PlantedGroundReady(item);
    }
    public void changeToEmpty(int x, int y){
        log.info("changed to PlantedGroundReady");
        gridPlanted[x][y] = new Empty();
    }

    public void changeToGrass(int x, int y){
        grid[x][y] = new Grass();
    }


        /*
        #####COMMENT########
        Normal filLWorld. Only Grass or fields are possible so far.
         */

    public void fillWorld(int[][] groundIndex) {
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid.length; j++) {
                if(groundIndex[i][j] == 1){
                    Ground g = new Grass();
                    grid[i][j] = g;
                } else if (groundIndex[i][j] == 2) {
                    Ground f = new Field();
                    grid[i][j] = f;

                }
            }

        }
    }
    /*
       #####COMMENT########
       Normal filLWorldPlanted. this is used for a new World. So no planted fields are excpected.
        */
    public void fillPlanted(int[][] cropIndex, Player player) {
        for (int i = 0; i < gridPlanted.length; i++) {
            for (int j = 0; j < gridPlanted.length; j++) {
                if(cropIndex[i][j] == 404){
                    Ground e = new Empty();
                    gridPlanted[i][j] = e;
                }
            }
        }
    }
     /*
        #####COMMENT########
         filLWorld when a game is loaded. This method creates classes for the Array with crops.
         Beside the ground index the method has to know wich item is planted.
         So first the GroundType is read.
         Then the cropType.
         When a crop was in grow process (index 8) a new Grow Process is called. Thats why all growing crops hav to start from 0 when a game in loaded.
         */

    public void fillPlantedLoaded(int[][] cropIndex, int[] growingCropType, int[] cropType, Player player) {
        log.info("fillPlantedLoaded called");
        int counter = 0;
        int growingCropCounter = 0;
        for (int i = 0; i < gridPlanted.length; i++) {
            for (int j = 0; j < gridPlanted.length; j++) {
                if(cropIndex[i][j] == 404){
                    Ground e = new Empty();
                    gridPlanted[i][j] = e;
                } else if (cropIndex[i][j] == 8) {
                    log.info("Index 8 true");
                    if(growingCropType[growingCropCounter] == 1){
                        Item wheat  = player.getWheatItem();
                        Ground pg = new PlantedGround(wheat);
                        gridPlanted[i][j] = pg;
                        GrowProcesses growProcesses = new GrowProcesses(this , i, j,wheat);
                        growingCropCounter++;
                    } else if (growingCropType[growingCropCounter] == 2) {
                        Item potato  = player.getPotatoItem();
                        Ground pg = new PlantedGround(potato);
                        gridPlanted[i][j] = pg;
                        GrowProcesses growProcesses = new GrowProcesses(this , i, j,potato);
                        growingCropCounter++;
                    }


                } else if (cropIndex[i][j] == 81) {
                    if(cropType[counter] == 1){
                        Item wheat  = player.getWheatItem();
                        Ground pgr = new PlantedGroundReady(wheat);
                        gridPlanted[i][j] = pgr;
                        counter++;
                    } else if (cropType[counter] == 2) {
                        Item potato  = player.getPotatoItem();
                        Ground pgr = new PlantedGroundReady(potato);
                        gridPlanted[i][j] = pgr;
                        counter++;
                    }


                }
            }
        }
    }

    public int calculateCoordinates (int c){
        return (c * 50);
    }

    public int[][] getDefaultGroundMap() {
        return defaultGroundMap;
    }

    public int[][] getDefaultPlantedMap() {
        return defaultPlantedMap;
    }

}