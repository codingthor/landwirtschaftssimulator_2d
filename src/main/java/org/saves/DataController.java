package org.saves;

import org.Player;
import org.World;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ground.Ground;
import org.vehicles.*;

import java.io.*;


public class DataController {
    Player p;
    World w;
    VehicleManager vm;

    public DataController(Player p, World w, VehicleManager vehicleManager){

        this.p = p;
        this.w = w;
        this.vm = vehicleManager;
    }



    private static final Logger log = LogManager.getLogger(DataController.class);


    public void save(String game){
        //the selected Game Name is referenced to teh correct .dat File name
        String s = gameFinder(game);
        try {

            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(s));
            DataSave ds = new DataSave();
            //simple stats are saved
            ds.playerY = p.getY();
            ds.playerX = p.getX();
            ds.currency = p.getCurrencyItem().getAmount();
            ds.wheat = p.getWheatItem().getAmount();
            ds.potato = p.getPotatoItem().getAmount();

            Ground[][] groundGrid = w.getGrid();
            Ground[][] plantedGrid = w.getGridPlanted();


            ds.groundMap = new int[plantedGrid.length][plantedGrid[0].length];
            ds.plantedMap = new int[plantedGrid.length][plantedGrid[0].length];

            //Here we count how many cropsReady and how many cropsGrowing exist in a world.
            int cropCounter = 0;
            int growingCropCounter = 0;

            for (int i = 0; i < plantedGrid.length; i++){
                for(Ground g: plantedGrid[i]){
                    if(g.getIndex()==81){
                        cropCounter++;
                    } else if (g.getIndex()==8) {
                        growingCropCounter++;
                    }
                }

            }
            log.info("cropcounter: " + cropCounter);
            log.info("Growing cropcounter: " + growingCropCounter);

                //new Arrays to save all crops.
                ds.cropTypes = new int[cropCounter];
                ds.growingCropTypes = new int[growingCropCounter];

            cropCounter = 0;
            growingCropCounter = 0;



             /*
                 #####COMMENT########
                        the maps get the groundIndexe in the world. (this indexes get interpreted as ground Classes in World.fillWorldLoaded())
                           if the ground is a planted Ground (index 8 or 81) the cropy type gets saved in the cropTypeArray.
                           Since .dat files cant save string we have to call the method pathAsIndex() wich returns for potatos 2 and for wheat 1.
                           This i also interpreted in the world.fillWorldLoaded() Method.

         */

                for (int i = 0; i < groundGrid.length; i++) {
                    for (int j = 0; j < groundGrid.length; j++) {
                        ds.groundMap[i][j] = groundGrid[i][j].getIndex();
                        ds.plantedMap[i][j] = plantedGrid[i][j].getIndex();

                    if(plantedGrid[i][j].getIndex() == 81){
                         ds.cropTypes[cropCounter] = pathAsIndex(plantedGrid[i][j].getImagePath());
                        cropCounter++;
                    } else if (plantedGrid[i][j].getIndex() == 8) {
                        ds.growingCropTypes[growingCropCounter] = pathAsIndex(plantedGrid[i][j].getImagePath());
                        growingCropCounter++;
                    }
                    }
                }

            ds.hasTractor = p.getHasTractor();
            ds.hasHarvester = p.getHasHarvester();
            ds.hasATV = p.getHasATV();


            VehicleClass[] vehicleCollection = vm.getVehicleCollection();
            ds.vehicleCoordinates = new double[vehicleCollection.length][2];

            //Vehicle coordinates saved. Because the factory spawns them minus their size, we have to add the size here.

            for (VehicleClass v : vehicleCollection) {
                if (v.getImg() == "Tractor") {
                    ds.vehicleCoordinates[0][0] = v.getX() + v.getSeize();
                    ds.vehicleCoordinates[0][1] = v.getY() + v.getSeize();
                } else if (v.getImg() == "Harvester") {
                    ds.vehicleCoordinates[1][0] = v.getX() + v.getSeize();
                    ds.vehicleCoordinates[1][1] = v.getY() + v.getSeize();
                } else if (v.getImg() == "ATV") {
                    ds.vehicleCoordinates[2][0] = v.getX() + v.getSeize();
                    ds.vehicleCoordinates[2][1] = v.getY() + v.getSeize();
                }
            }


            oos.writeObject(ds);
            log.info("Game saved");
        }
        catch(Exception e){
            log.warn("Save Exception! Game not saved"+ e);
        }
    }

    public void load(String game){

        String s = gameFinder(game);


        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(s));
            DataSave ds = (DataSave)ois.readObject();
            //all relevant stats are setted
            p.setY(ds.playerY);
            p.setX(ds.playerX);
            p.getCurrencyItem().setAmount(ds.currency);
            p.getWheatItem().setAmount(ds.wheat);
            p.getPotatoItem().setAmount(ds.potato);

            w.fillWorld(ds.groundMap);
            w.fillPlantedLoaded(ds.plantedMap, ds.growingCropTypes, ds.cropTypes, p);


            p.setHasHarvester(ds.hasHarvester);
            p.setHasTractor(ds.hasTractor);
            p.setHasATV(ds.hasATV);


            //new vehicles declared and setted to saved position to make sure the coordiantes from game 1 save do not appear in game 2 save.
            vm.getVehicleCollection()[0] = new Tractor();
            vm.getVehicleCollection()[1] = new Harvester();
            vm.getVehicleCollection()[2] = new ATV();




            if(ds.hasTractor){
                vm.getVehicleCollection()[0].setX(ds.vehicleCoordinates[0][0]);
                vm.getVehicleCollection()[0].setY(ds.vehicleCoordinates[0][1]);
            }

            if(ds.hasHarvester){
                vm.getVehicleCollection()[1].setX(ds.vehicleCoordinates[1][0]);
                vm.getVehicleCollection()[1].setY(ds.vehicleCoordinates[1][1]);
            }

            if(ds.hasATV){
                vm.getVehicleCollection()[2].setX(ds.vehicleCoordinates[2][0]);
                vm.getVehicleCollection()[2].setY(ds.vehicleCoordinates[2][1]);
            }
            log.info("Game loaded");


        }
        catch(Exception e){
            log.warn("Save Exception! Game not loaded" + e);
        }

    }

    public String gameFinder(String st){
        String s = "";
        switch (st){
            case"Game 1":
                s = "save1.dat";
                return s;

            case"Game 2":
                s = "save2.dat";
                return s;

            case"Game 3":
                s = "save3.dat";
                return s;

            case"Back Up":
                s = "backup.dat";
                return s;
        }
        return s;
    }

    public int pathAsIndex(String path){
        if(path == "PotatoGrowing" || path == "PotatoReady"){
            return 2;
        } else if(path == "WheatGrowing" || path == "WheatReady"){
            return 1;
        }
        return 0;
    }

}
