package org.saves;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;


public class DataSave implements Serializable {

    private static final Logger log = LogManager.getLogger(DataSave.class);


    double playerX;
    double playerY;
    int currency;
    int wheat;
    int potato;
    int[][] groundMap;
    int[][] plantedMap;
    boolean hasTractor;
    boolean hasHarvester;
    boolean hasATV;
    double[][] vehicleCoordinates;
    int[] cropTypes;
    int[] growingCropTypes;



}
