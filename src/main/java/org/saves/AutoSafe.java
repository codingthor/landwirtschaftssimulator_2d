package org.saves;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class AutoSafe {
 DataController dc;
 Thread dataThread;


    private static final Logger log = LogManager.getLogger(AutoSafe.class);

    public AutoSafe(DataController dc){
        log.info("AutoSafe called");
        this.dc = dc;

     dataThread = new Thread(){
         private ScheduledExecutorService scheduler;

         @Override
      public void run() {
       log.info("Thread Run");
          this.scheduler = Executors.newScheduledThreadPool(1);
          scheduler.scheduleAtFixedRate(helloRunnable, 8, 8, TimeUnit.SECONDS);

      }
     };

    }

    Runnable helloRunnable = new Runnable() {
        public void run() {
            dc.save("Back Up");
            log.info("Back Up made. 8 Seconds");
        }
    };

    public void startBackUp(){
        try {
        dataThread.start();
        } catch(IllegalThreadStateException illegalThreadStateException) {
            log.error("Backup Thread already active: " + illegalThreadStateException);
        }
    }
    public void saveCall(String s){
        dc.save("Back Up");

    }



}
