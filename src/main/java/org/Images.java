package org;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.util.HashMap;
import javafx.scene.image.Image;
import java.io.FileInputStream;


public class Images {
    private static FileInputStream fpsGrass;
    private static Image imageGrass;
    private static FileInputStream fpsField;
    private static Image imageField;
    private static FileInputStream fpsWheatGrowing;
    private static Image imageWheatGrowing;
    private static FileInputStream fpsWheatReady;
    private static Image imageWheatReady;
    private static FileInputStream fpsPotatoGrowing;
    private static Image imagePotatoGrowing;
    private static FileInputStream fpsPotatoReady;
    private static Image imagePotatoReady;
    private static FileInputStream fpsEmpty;
    private static Image imageEmpty;
    private static FileInputStream fpsPlayer;
    private static Image imagePlayer;
    private static FileInputStream fpsCoin;
    private static Image imageCoin;
    private static FileInputStream fpsWheatIcon;
    private static FileInputStream fpsWheatIconInv;
    private static Image imageWheatIcon;
    private static Image imageWheatIconInv;
    private static FileInputStream fpsPotatoIcon;
    private static FileInputStream fpsPotatoIconInv;
    private static Image imagePotatoIcon;
    private static Image imagePotatoIconInv;
    private static FileInputStream fpsInvActive1;
    private static Image imageInvActive1;
    private static FileInputStream fpsInvActive2;
    private static Image imageInvActive2;
    private static FileInputStream fpsInvActive3;
    private static Image imageInvActive3;
    private static FileInputStream fpsInvActive4;
    private static Image imageInvActive4;
    private static FileInputStream fpsHandIcon;
    private static Image imageHandIcon;
    private static FileInputStream fpsPlowIcon;
    private static Image imagePlowIcon;
    private static FileInputStream fpsShop;
    private static Image imageShop;
    private static FileInputStream fpsTractor;
    private static Image imageTractor;

    private static FileInputStream fpsHarvester;
    private static Image imageHarvester;

    private static FileInputStream fpsATV;
    private static Image imageATV;

    private static HashMap<String, Image> imageMap;

    Images() throws FileNotFoundException {
        fpsGrass = new FileInputStream("src/main/resources/images/grass.png");
        imageGrass = new Image(fpsGrass, 50, 50, true, true);

        fpsField = new FileInputStream("src/main/resources/images/field.png");
        imageField = new Image(fpsField, 50, 50, true, true);

        fpsWheatGrowing = new FileInputStream("src/main/resources/images/wheatgrowing.png");
        imageWheatGrowing = new Image(fpsWheatGrowing, 50, 50, true, true);

        fpsWheatReady = new FileInputStream("src/main/resources/images/wheatready.png");
        imageWheatReady = new Image(fpsWheatReady, 50, 50, true, true);

        fpsPotatoGrowing = new FileInputStream("src/main/resources/images/potatogrowing.png");
        imagePotatoGrowing = new Image(fpsPotatoGrowing, 50, 50, true, true);

        fpsPotatoReady = new FileInputStream("src/main/resources/images/potatoready.png");
        imagePotatoReady = new Image(fpsPotatoReady, 50, 50, true, true);

        fpsEmpty = new FileInputStream("src/main/resources/images/empty2.png");
        imageEmpty = new Image(fpsEmpty, 50, 50, true, true);

        fpsPlayer = new FileInputStream("src/main/resources/images/player.png");
        imagePlayer = new Image(fpsPlayer, 50, 50, true, true);

        fpsCoin = new FileInputStream("src/main/resources/images/Items/coin.png");
        imageCoin = new Image(fpsCoin, 30, 30, true, true);

        fpsWheatIcon = new FileInputStream("src/main/resources/images/Items/wheatInv.png");
        imageWheatIcon = new Image(fpsWheatIcon, 30, 30, true, true);

        fpsWheatIconInv = new FileInputStream("src/main/resources/images/Items/wheatInv.png");
        imageWheatIconInv = new Image(fpsWheatIconInv, 50, 50, true, true);

        fpsPotatoIcon = new FileInputStream("src/main/resources/images/Items/potatoInv.png");
        imagePotatoIcon = new Image(fpsPotatoIcon, 30, 30, true, true);

        fpsPotatoIconInv = new FileInputStream("src/main/resources/images/Items/potatoInv.png");
        imagePotatoIconInv = new Image(fpsPotatoIconInv, 50, 50, true, true);

        fpsInvActive1 = new FileInputStream("src/main/resources/images/active1.png");
        imageInvActive1 = new Image(fpsInvActive1, 290, 70, true, true);

        fpsInvActive2 = new FileInputStream("src/main/resources/images/active2.png");
        imageInvActive2 = new Image(fpsInvActive2, 290, 70, true, true);

        fpsInvActive3 = new FileInputStream("src/main/resources/images/active3.png");
        imageInvActive3 = new Image(fpsInvActive3, 290, 70, true, true);

        fpsInvActive4 = new FileInputStream("src/main/resources/images/active4.png");
        imageInvActive4 = new Image(fpsInvActive4, 290, 70, true, true);

        fpsHandIcon = new FileInputStream("src/main/resources/images/items/hand.png");
        imageHandIcon = new Image(fpsHandIcon, 50, 50, true, true);

        fpsPlowIcon = new FileInputStream("src/main/resources/images/items/plow.png");
        imagePlowIcon = new Image(fpsPlowIcon, 50, 50, true, true);

        fpsShop = new FileInputStream("src/main/resources/images/shop.png");
        imageShop = new Image(fpsShop, 100, 100, true, true);

        fpsTractor = new FileInputStream("src/main/resources/images/tractor.png");
        imageTractor = new Image(fpsTractor, 100, 100, true, true);

        fpsHarvester = new FileInputStream("src/main/resources/images/harvester.png");
        imageHarvester = new Image(fpsHarvester, 100, 100, true, true);

        fpsATV = new FileInputStream("src/main/resources/images/quad.png");
        imageATV = new Image(fpsATV, 100, 100, true, true);

        imageMap = new HashMap<>();

        imageMap.put("Grass", imageGrass);
        imageMap.put("Field", imageField);
        imageMap.put("WheatGrowing", imageWheatGrowing);
        imageMap.put("WheatReady", imageWheatReady);
        imageMap.put("PotatoGrowing", imagePotatoGrowing);
        imageMap.put("PotatoReady", imagePotatoReady);
        imageMap.put("Dot", imageEmpty);
        imageMap.put("Player", imagePlayer);
        imageMap.put("Coin", imageCoin);
        imageMap.put("WheatIcon", imageWheatIcon);
        imageMap.put("WheatIconInv", imageWheatIconInv);
        imageMap.put("PotatoIcon", imagePotatoIcon);
        imageMap.put("PotatoIconInv", imagePotatoIconInv);
        imageMap.put("InvActive1", imageInvActive1);
        imageMap.put("InvActive2", imageInvActive2);
        imageMap.put("InvActive3", imageInvActive3);
        imageMap.put("InvActive4", imageInvActive4);
        imageMap.put("HandIcon", imageHandIcon);
        imageMap.put("PlowIcon", imagePlowIcon);
        imageMap.put("Shop", imageShop);
        imageMap.put("Tractor", imageTractor);
        imageMap.put("Harvester", imageHarvester);
        imageMap.put("ATV", imageATV);
    }

    private static final Logger log = LogManager.getLogger(Images.class);

    public static HashMap<String, Image> getImageMap() {
        return imageMap;
    }
}