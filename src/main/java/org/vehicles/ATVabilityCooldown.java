package org.vehicles;

import org.Player;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Timer;
import java.util.TimerTask;


public class ATVabilityCooldown implements Runnable{
 Timer timer;
 TimerTask task;

 Boolean cd;

    private static final Logger log = LogManager.getLogger(ATVabilityCooldown.class);

    public void abilityCooldown(int cd, ATV v, String s, Player p){
        log.info("ATV Ability Cooldown called with: " + cd + " cd as " + s );

        timer = new Timer();
        task = new TimerTask() {
            @Override
            public void run() {
                if(v.getCurrentState() == "active"){
                    v.setSpeed(3.5);
                    p.setSpeed(v.getSpeed());
                }
                v.setCurrentState(s);





                log.info("ATV Cooldown exit");

            }
        };
        timer.schedule(task, cd);


    }

 @Override
 public void run() {

 }
}
