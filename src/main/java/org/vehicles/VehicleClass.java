package org.vehicles;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.Group;
import org.Entity;
import org.World;
import org.Player;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public abstract class VehicleClass extends Entity implements vehicle {
 private Integer seize;
 private Double speed;



 private Group vehicleGroup;
 BooleanProperty driveVertical = new SimpleBooleanProperty(false);
 BooleanProperty driveHorizontal = new SimpleBooleanProperty(false);



    private static final Logger log = LogManager.getLogger(VehicleClass.class);

    public void setSpeed(Double speed){
     this.speed = speed;
    }

 public void setSeize(Integer seize) {
  this.seize = seize;
 }



 public Group getVehicleGroup() {
  return vehicleGroup;
 }

 public void setVehicleGroup(Group vehicleGroup) {
  this.vehicleGroup = vehicleGroup;
 }

 public BooleanProperty getDriveVertical(){
  return driveVertical;
 }

 public BooleanProperty getDriveHorizontal(){
  return driveHorizontal;
 }

 public Double getSpeed() {
  return speed;
 }

 @Override
 public void vehicleAbility(World w, Player p) {

 }

 @Override
 public void drive() {

 }

 @Override
 public int getSeize() {
     return seize;

 }
}
