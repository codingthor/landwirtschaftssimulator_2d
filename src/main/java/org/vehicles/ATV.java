package org.vehicles;

import org.Player;
import org.World;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ATV extends VehicleClass implements vehicle{



 private String currentState;
 private final ATVabilityCooldown timer;


 public ATV(){
  setImg("ATV");
  setSeize(30);
  setSpeed(3.5);
  setCurrentState("available");
  timer = new ATVabilityCooldown();
 }

 @Override
 public void vehicleAbility(World w, Player p) {

if(this.currentState == "available"){
 setSpeed(10.0);
 p.setSpeed(getSpeed());
 setCurrentState("active");
 timer.abilityCooldown(3000, this, "onCooldown", p);
 timer.abilityCooldown(13000, this, "available", p);



}
else if(this.currentState == "active"){

}
else if(this.currentState == "onCooldown"){
 setSpeed(3.5);
 p.setSpeed(getSpeed());

}




 }

 public void setCurrentState(String s){
  this.currentState = s;
 }

 public String getCurrentState(){
  return this.currentState;
 }

 private static final Logger log = LogManager.getLogger(ATV.class);


 @Override
 public void drive() {

 }

}
