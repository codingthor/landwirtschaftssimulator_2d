package org.vehicles;

import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;


public class VehicleFactory {
    Pane entityPane;
    VehicleManager vm;
    ImageView atvIV;
    ImageView harvesterIV;
    ImageView tractorIV;
    HashMap<String, Image> imageMap;
    Group vehicleGroup;

    public VehicleFactory(Pane pane, VehicleManager vehicleManager, HashMap<String, Image> imageMap, ImageView atvImageView, ImageView harvesterImageView, ImageView tractorImageView){
        this.entityPane = pane;
        this.vm = vehicleManager;
        this.imageMap = imageMap;
        this.tractorIV = tractorImageView;
        this.harvesterIV = harvesterImageView;
        this.atvIV = atvImageView;


    }

    private static final Logger log = LogManager.getLogger(VehicleFactory.class);

    public void createVehicle(String vehicletype, int x, int y){
        int vehicleCollectionIndex = 0;
        VehicleClass[] vehicleCollection = vm.getVehicleCollection();
        vehicleGroup = new Group();

        switch (vehicletype){
            case "Tractor":
                vehicleCollectionIndex = 0;
                this.tractorIV.setImage(imageMap.get(vehicleCollection[0].getImg()));
                vehicleGroup.getChildren().addAll(tractorIV);

                break;

            case "Harvester":
                vehicleCollectionIndex = 1;
                this.harvesterIV.setImage(imageMap.get(vehicleCollection[1].getImg()));
                vehicleGroup.getChildren().addAll(harvesterIV);

                break;

            case "ATV":
                vehicleCollectionIndex = 2;
                this.atvIV.setImage(imageMap.get(vehicleCollection[2].getImg()));
                vehicleGroup.getChildren().addAll(atvIV);

                break;

        }
        vehicleCollection[vehicleCollectionIndex].setX(x - vehicleCollection[vehicleCollectionIndex].getSeize());
        vehicleCollection[vehicleCollectionIndex].setY(y - vehicleCollection[vehicleCollectionIndex].getSeize());
        vehicleCollection[vehicleCollectionIndex].setVehicleGroup(vehicleGroup);
        vehicleCollection[vehicleCollectionIndex].getVehicleGroup().relocate(vehicleCollection[vehicleCollectionIndex].getX(), vehicleCollection[vehicleCollectionIndex].getY());
        entityPane.getChildren().add(vehicleCollection[vehicleCollectionIndex].getVehicleGroup());



    }



}
