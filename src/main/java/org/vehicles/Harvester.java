package org.vehicles;

import org.Player;
import org.World;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ground.Ground;
import org.ground.crops.PlantedGroundReady;


public class Harvester extends VehicleClass implements vehicle {

    private final Integer[] playerX = new Integer[3];
    private final Integer[] playerY = new Integer[3];
    private final Integer[] gridType = new Integer[3];
    private final Integer[] gridTypePlanted = new Integer[3];
    private final PlantedGroundReady[] gridReadyGroundType = new PlantedGroundReady[3];

    private static final Logger log = LogManager.getLogger(Harvester.class);

    public Harvester(){
     setImg("Harvester");
     setSeize(50);
     setSpeed(1.5);
    }

 @Override
 public void vehicleAbility(World w, Player p) {



     playerX[0] = p.calculateCoordinates(p.getX());
     playerY[0] = p.calculateCoordinates(p.getY());
     gridType[0] = w.getGridType(playerX[0], playerY[0]);
     gridTypePlanted[0] = w.getGridPlantedType(playerX[0], playerY[0]);
     if(driveVertical.get()) {
         playerX[1] = p.calculateCoordinates(p.getX())-1;
         playerX[2] = p.calculateCoordinates(p.getX())+1;
         try {
         gridType[1] = w.getGridType(playerX[1], playerY[0]);
         } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException){
             gridType[1] = 0;
             log.warn("GridType called for Coordinates out of Game World." + arrayIndexOutOfBoundsException);
         }
         try {
         gridType[2] = w.getGridType(playerX[2], playerY[0]);
        } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException){
         gridType[1] = 0;
         log.warn("GridType called for Coordinates out of Game World." + arrayIndexOutOfBoundsException);
            }
         try {
         gridTypePlanted[1] = w.getGridPlantedType(playerX[1], playerY[0]);
         } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException){
             gridType[1] = 0;
             log.warn("GridType called for Coordinates out of Game World." + arrayIndexOutOfBoundsException);
         }
         try {
             gridTypePlanted[2] = w.getGridPlantedType(playerX[2], playerY[0]);
         }catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException){
                 gridType[1] = 0;
                 log.warn("GridType called for Coordinates out of Game World." + arrayIndexOutOfBoundsException);
             }

         for (int i = 0; i < gridType.length; i++) {
             log.debug("Harvester method called" + gridType.length);
             if (gridType[i] == 2 && gridTypePlanted[i] ==81) {
                 log.debug("Harvester method called - if condotion true");
                 Ground[][] gridPlanted = w.getGridPlanted();
                 gridReadyGroundType[i] = (PlantedGroundReady) gridPlanted[playerX[i]][playerY[0]];
                 w.changeToEmpty(playerX[i], playerY[0]);
                 w.changeToGrass(playerX[i], playerY[0]);
                 gridReadyGroundType[i].getItem().setAmount(gridReadyGroundType[i].getItem().getAmount()+2);

             }
         }
     }
     if(driveHorizontal.get()) {
         playerY[1] = p.calculateCoordinates(p.getY())-1;
         playerY[2] = p.calculateCoordinates(p.getY())+1;
         try {
             gridType[1] = w.getGridType(playerX[0], playerY[1]);
         } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException){
             gridType[1] = 0;
             log.warn("GridType called for Coordinates out of Game World." + arrayIndexOutOfBoundsException);
         }
             try {
                 gridType[2] = w.getGridType(playerX[0], playerY[2]);
             } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException){
                     gridType[1] = 0;
                     log.warn("GridType called for Coordinates out of Game World." + arrayIndexOutOfBoundsException);
                 }
         try {
         gridTypePlanted[1] = w.getGridPlantedType(playerX[0], playerY[1]);
         } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException){
             gridType[1] = 0;
             log.warn("GridType called for Coordinates out of Game World." + arrayIndexOutOfBoundsException);
         }

             try {
         gridTypePlanted[2] = w.getGridPlantedType(playerX[0], playerY[2]);
             } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException){
                 gridTypePlanted[1] = 0;
                 log.warn("GridTypePlanted called for Coordinates out of Game World." + arrayIndexOutOfBoundsException);
             }
         for (int i = 0; i < gridType.length; i++) {
             log.debug("Harvester method called - horizontal");
             if (gridType[i] == 2 && gridTypePlanted[i] ==81) {
                 log.debug("Harvester method called - horizontal- if condotion true");
                 Ground[][] gridPlanted = w.getGridPlanted();
                 gridReadyGroundType[i] = (PlantedGroundReady) gridPlanted[playerX[0]][playerY[i]];
                 w.changeToEmpty(playerX[0], playerY[i]);
                 w.changeToGrass(playerX[0], playerY[i]);
                 gridReadyGroundType[i].getItem().setAmount(gridReadyGroundType[i].getItem().getAmount()+2);

             }
         }
     }


 }

 @Override
 public void drive() {

 }


}
