package org.vehicles;

import org.Player;
import org.World;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Tractor extends VehicleClass implements vehicle {


 private final Integer[] playerX = new Integer[3];
 private final Integer[] playerY = new Integer[3];
 private final Integer[] gridType = new Integer[3];




 public Tractor(){
  setImg("Tractor");
  setSeize(50);
  setSpeed(1.5);


 }





    private static final Logger log = LogManager.getLogger(Tractor.class);

 @Override
 public void vehicleAbility(World w, Player p) {
  log.debug("Tractor Ability called");

  playerX[0] = p.calculateCoordinates(p.getX());
  playerY[0] = p.calculateCoordinates(p.getY());
  gridType[0] = w.getGridType(playerX[0], playerY[0]);
  if(driveVertical.get()) {

    playerX[1] = p.calculateCoordinates(p.getX()) - 1;
    playerX[2] = p.calculateCoordinates(p.getX()) + 1;
   try {
    gridType[1] = w.getGridType(playerX[1], playerY[0]);
   } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException){
    gridType[1] = 0;
    log.warn("GridType called for Coordinates out of Game World." + arrayIndexOutOfBoundsException);
   }
   try {
    gridType[2] = w.getGridType(playerX[2], playerY[0]);
   } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException){
    gridType[1] = 0;
    log.warn("GridType called for Coordinates out of Game World." + arrayIndexOutOfBoundsException);
   }
   for (int i = 0; i < gridType.length; i++) {
    if (gridType[i] == 1) {
     w.changeToField(playerX[i], playerY[0]);
    }
   }
  }
  if(driveHorizontal.get()) {
   try {
   playerY[1] = p.calculateCoordinates(p.getY())-1;
   playerY[2] = p.calculateCoordinates(p.getY())+ 1;
    try {
   gridType[1] = w.getGridType(playerX[0], playerY[1]);
    } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException){
     gridType[1] = 0;
     log.warn("GridType called for Coordinates out of Game World." + arrayIndexOutOfBoundsException);
    }
    try {
   gridType[2] = w.getGridType(playerX[0], playerY[2]);
   } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException){
    gridType[1] = 0;
    log.warn("GridType called for Coordinates out of Game World." + arrayIndexOutOfBoundsException);
   }
   } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException){
    log.warn("createImgView called for Coordinates out of Game World." + arrayIndexOutOfBoundsException);
   }
   for (int i = 0; i < gridType.length; i++) {
    if (gridType[i] == 1) {
     w.changeToField(playerX[0], playerY[i]);
    }
   }
  }




  }



 @Override
 public void drive() {

 }


}
