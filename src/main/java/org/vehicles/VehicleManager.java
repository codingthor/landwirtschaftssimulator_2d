package org.vehicles;

import javafx.scene.Group;
import org.Player;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class VehicleManager {

  private VehicleClass activeVehicle;

  private Group activeVehicleGroup;
  private final VehicleClass[] vehicleCollection;

  public VehicleManager(){
    log.info("Vehicle Manager called");

    //vehicle Manager always has all cars in it.

    vehicleCollection = new VehicleClass[3];
    vehicleCollection[0] = new Tractor();
    vehicleCollection[1]= new Harvester();
    vehicleCollection[2]= new ATV();

  }


    private static final Logger log = LogManager.getLogger(VehicleManager.class);

  public VehicleClass[] getVehicleCollection() {
    return vehicleCollection;
  }

  public void setActiveVehicle(VehicleClass v){
    log.info("Active Vehicle setted");
    this.activeVehicle = v;

  }

  public VehicleClass getActiveVehicle() {
    return activeVehicle;
  }


  public void setActiveVehicleGroup(Group g){
    this.activeVehicleGroup = g;
  }

  public Group getActiveVehicleGroup() {
    return activeVehicleGroup;
  }

  public void clearActiveVehicle(){
    this.activeVehicle = null;
  }

  public void clearActiveVehicleGroup(){
    this.activeVehicleGroup = null;
  }

  public void  sattleOn(Player p) {
    log.info("Sattle OnMethod called");

    if(p.getOnVehicle()){
      p.setOnVehicle(false);
      p.setSpeed(p.getDefaultSpeed());
      log.info("Player sattled Off");
      clearActiveVehicle();
      clearActiveVehicleGroup();
    }
    else if(!p.getOnVehicle()){
      int pX = p.getX();
      int pY = p.getY();
      for (int i = 0; i< this.vehicleCollection.length; i++){
        VehicleClass vC = this.vehicleCollection[i];
        int vX = vC.getX();
        int vY = vC.getY();
        int vS = vC.getSeize();
        if(vX <= pX+50 && vX+vS >= pX && vY <= pY+50 && vY+vS >= pY){
          p.setOnVehicle(true);
          p.setX(vX);
          p.setY(vY);
          p.setSpeed(vC.getSpeed());
          log.info("Player Sattled on");
          setActiveVehicle(vC);
          setActiveVehicleGroup(vC.getVehicleGroup());
          break;
        }

      }
    }

  }

}
