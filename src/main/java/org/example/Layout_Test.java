package org.example;


import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javafx.scene.layout.StackPane;

import java.io.FileInputStream;

public class Layout_Test extends Application{


Button button;
TextField textField;
CheckBox checkBox;
RadioButton rb1;
 RadioButton rb2;
 RadioButton rb3;
 ComboBox comboBox;

 Image image;
 Image imageTest;
 ImageView imgView2;
 ImageView imgView;

 FileInputStream fps;
 FileInputStream fpsTest;


 @Override
 public void start(Stage myFirstStage) throws Exception {
  myFirstStage.setTitle("Meine erste Stage");


  /* #########################################################################
     Hier bitte den Pfad der FileInputStreams an das eigene Projekt anpassen!
  #########################################################################*/
  fps = new FileInputStream("src/main/resources/images/test.png");
  fpsTest = new FileInputStream("src/main/resources/images/grass.png");
  image = new Image(fps, 50, 50, true, true);
  imageTest = new Image(fpsTest, 50, 50, true, true);
  imgView = new ImageView();
  imgView2 = new ImageView();



  imgView.setImage(imageTest);
  imgView2.setImage(image);


  StackPane rootPane = new StackPane();

  GridPane myLayout = new GridPane();
  myLayout.setPadding(new Insets(10, 10, 10, 10));

  GridPane myLayout2 = new GridPane();
  myLayout2.setPadding(new Insets(10, 10, 10, 10));

  rootPane.getChildren().addAll(myLayout, myLayout2);






  myLayout.add(imgView, 1,1);
  myLayout2.add(imgView2, 1,2);







  Scene myScene = new Scene(rootPane);
  myFirstStage.setScene(myScene);
  myFirstStage.show();

 }

 public static void main(String[] args) {
launch(args);
 }

}
