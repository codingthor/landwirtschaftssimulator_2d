package org.example;

import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.io.FileInputStream;


public class Vortrag_Paul extends Application {

    private static final Logger log = LogManager.getLogger(Vortrag_Paul.class);
 Button button, button2;
 Scene scene1, scene2;
 Text text1;

 @Override
 public void start(Stage stage) throws Exception {

  //Button 1
  button = new Button();
  button.setText("Klick mich");
  button.setOnAction(e -> stage.setScene(scene2));

  //Button2
  button2 = new Button();
  button2.setText("Zurück");
  button2.setOnAction(e -> stage.setScene(scene1));


  //Text1
  text1 = new Text();
  text1.setStyle("-fx-font-size: 15;");


  //Layout1
  VBox layout1 = new VBox();
  layout1.getChildren().addAll(button, text1);
  scene1 = new Scene(layout1, 400, 300);
  scene1.setOnKeyPressed(e -> {
   if(e.getCode()== KeyCode.W){
    text1.setText("Hallo!");
  }});

  //Layout2
  StackPane layout2 = new StackPane();
  layout2.getChildren().add(button2);
  scene2 = new Scene(layout2, 600, 250);

  //Stage
  stage.setTitle("Window");
  stage.setScene(scene1);
  stage.show();
 }


 public static void main(String[] args) {
  launch(args);
 }

}
