package org.example;


import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.Pane;

import java.io.FileInputStream;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;

public class PlayerALyout_Test extends Application{


 Button button;
 TextField textField;
 CheckBox checkBox;
 RadioButton rb1;
 RadioButton rb2;
 RadioButton rb3;
 ComboBox comboBox;

 Image image;
 Image imageTest;
 ImageView imgView2;
 ImageView imgView;

 FileInputStream fps;
 FileInputStream fpsTest;



 @Override
 public void start(Stage myFirstStage) throws Exception {
  myFirstStage.setTitle("Meine erste Stage");


  /* #########################################################################
     Hier bitte den Pfad der FileInputStreams an das eigene Projekt anpassen!
  #########################################################################*/


  FileInputStream playerFPS = new FileInputStream("src/main/resources/images/player.png");
  Image playerImage = new Image(playerFPS, 50, 50, true, true);
  ImageView playerImageView = new ImageView();
  playerImageView.setImage(playerImage);
  Group farmer = new Group(playerImageView);

  farmer.relocate(200, 200);



  StackPane rootPane = new StackPane();

  Pane myLayout = new Pane();
  myLayout.getChildren().addAll(farmer);



  rootPane.getChildren().addAll(myLayout);



  Scene myScene = new Scene(rootPane, 500, 500);

  myScene.setOnKeyPressed(e ->{
   switch(e.getCode()) {
    case W: farmer.relocate(farmer.getLayoutX(), farmer.getLayoutY()-2);
    break;
    case S: farmer.relocate(farmer.getLayoutX(), farmer.getLayoutY()+2);
     break;
    case A: farmer.relocate(farmer.getLayoutX()-2, farmer.getLayoutY());
     break;
    case D: farmer.relocate(farmer.getLayoutX()+2, farmer.getLayoutY());
     break;

   }
  });
  myFirstStage.setScene(myScene);
  myFirstStage.show();

 }

 public static void main(String[] args) {
  launch(args);
 }

}
