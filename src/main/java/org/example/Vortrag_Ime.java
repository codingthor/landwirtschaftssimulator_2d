package org.example;



import javafx.geometry.Insets;
import javafx.scene.control.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.geometry.Insets;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.FileInputStream;
import java.io.InputStream;

public class Vortrag_Ime extends Application{

 private static final Logger log = LogManager.getLogger(Vortrag_Ime.class);
Button button;
TextField textField;
CheckBox checkBox;
RadioButton rb1;
 RadioButton rb2;
 RadioButton rb3;
 ComboBox comboBox;

 Image image;
 ImageView imgView2;
 ImageView imgView;

 FileInputStream fps;


 @Override
 public void start(Stage myFirstStage) throws Exception {
  myFirstStage.setTitle("Meine erste Stage");

  button = new Button();
  button.setText("Klick mich");

  textField = new TextField();
  textField.appendText("Schreibe hier dein Text...");

  ToggleGroup rbGroup = new ToggleGroup();

  rb1 = new RadioButton();
  rb1.setText("Auswahl 1");
  rb1.setToggleGroup(rbGroup);
  rb1.setSelected(true);

  rb2 = new RadioButton();
  rb2.setText("Auswahl 2");
  rb2.setToggleGroup(rbGroup);

  rb3 = new RadioButton();
  rb3.setText("Auswahl 3");
  rb3.setToggleGroup(rbGroup);

  comboBox = new ComboBox<String>();
  comboBox.getItems().addAll("Name1", "Name 2", "Name 3", "Name 4");


/*

   #########################################################################
     Beispiel Code um ein Bild anzeigen zu lassen. Bei Verwendung bitten den Pfad des FileInputStream anpassen!
  fps = new FileInputStream("src/main/resources/images/test.png");
  image = new Image(fps, 50, 50, true, true);
  imgView = new ImageView();
  imgView.setImage(image);
 */



  GridPane myLayout = new GridPane();
  myLayout.setPadding(new Insets(10, 10, 10, 10));


  myLayout.setHgap(10);
  myLayout.setVgap(10);

  myLayout.add(button, 0, 0);
  myLayout.add(textField, 0, 1);
  myLayout.add(comboBox, 0, 2);
  myLayout.add(rb1, 0, 3);
  myLayout.add(rb2, 1, 3);
  myLayout.add(rb3, 2, 3);

  /*
  myLayout.add(imgView, 1,1);

*/





  Scene myScene = new Scene(myLayout, 400, 300);
  myFirstStage.setScene(myScene);
  myFirstStage.show();


 }
 public static void main(String[] args) {
launch(args);
 }

}
