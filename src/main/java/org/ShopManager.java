package org;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.vehicles.VehicleManager;

import java.util.HashMap;
import java.util.LinkedList;

public class ShopManager {
    private static final Logger log = LogManager.getLogger(ShopManager.class);

    private final Player player;
    private final LinkedList<Offer> offers =new LinkedList<>();
    private HashMap<String, Offer> offerMap;
    private final VehicleManager vm;


    ShopManager(Player player, VehicleManager vm){
        log.info("created ShopManager");
        this.player = player;
        this.vm = vm;
        initializeOffers();
    }

    private void initializeOffers(){
        log.info("initializeOffers called");
        offers.add(new Offer(player.getWheatItem(), 1,player.getCurrencyItem(), 15));
        offers.add(new Offer(player.getPotatoItem(), 1,player.getCurrencyItem(), 19));
        offers.add(new Offer(player.getCurrencyItem(), 12,player.getWheatItem(), 1));
        offers.add(new Offer(player.getCurrencyItem(), 16,player.getPotatoItem(), 1));
        offers.add(new Offer(player.getPlowItem(), 1,player.getCurrencyItem(), 500));
        offers.add(new Offer(player.getHandItem(), 1,player.getCurrencyItem(), 600));

        offerMap = new HashMap<>();
        offerMap.put("BuyWheat",offers.get(0));
        offerMap.put("BuyPotato",offers.get(1));
        offerMap.put("SellWheat",offers.get(2));
        offerMap.put("SellPotato",offers.get(3));
        offerMap.put("BuyPlow",offers.get(4));
        offerMap.put("BuyHand",offers.get(5));
    }

    public HashMap<String, Offer> getOfferMap() {
        return offerMap;
    }

    public Player getPlayer() {
        return player;
    }

    public void tradeLoop(Offer offer, int amount){
        log.info("called tradeLoop()");
        for (int i = 0; i<amount; i++){
            offer.trade();
            log.info("tradeloop " + i);
        }
    }
}
