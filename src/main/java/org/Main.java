package org;

import javafx.animation.AnimationTimer;
import javafx.application.Application;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ground.Ground;
import org.saves.AutoSafe;
import org.saves.DataController;
import org.vehicles.*;


import static java.lang.Math.sin;

import java.util.HashMap;


public class Main extends Application {
    ImageView invBackground;
    Ground[][] grid;
    Ground[][] gridPlanted;
    StackPane GameWorld;
    GridPane layerGround;
    GridPane layerCrop;
    StackPane rootPane;
    BorderPane overlay;
    StackPane inv;
    HBox invItems;
    VBox resources;
    HBox currency;
    HBox shopCurrency;
    HBox wheat;
    HBox potato;
    Scene mainScene;
    Pane layerPlayer;
    Pane layerShop;
    Group farmer;
    Group shop;
    Player p;
    Shop s;
    World w;
    AnimationTimer timer;
    Images images;
    MainController mainController;
    ShopController shopController;
    HashMap<String, Image> imageMap;

    //Shop GUI
    VBox shopLayout;
    BorderPane shopHeader;
    VBox shopWheat;
    HBox shopWheatButtons;
    VBox shopPotato;
    HBox shopPotatoButtons;
    HBox shopButtons1;
    VBox shopPlow;
    HBox shopPlowButtons;
    VBox shopHand;
    HBox shopHandButtons;
    HBox shopButtons2;
    HBox shopSellButtons;

    ImageView shopImgWheat;
    ImageView shopImgPotato;
    ImageView shopImgPlow;
    ImageView shopImgHand;
    StackPane shopPane;
    Scene shopScene;
    Stage shopStage;

    //Vehicle
    VehicleManager vehicleManager;
    VehicleFactory vehicleFactory;
    VehicleClass[] vehicleCollection;
    VehicleClass activeVehicle;
    Group activeVehicleGroup;
    ImageView tractorImageView;
    ImageView harvesterImageView;
    ImageView atvImageView;
    //Start Stage
    Button newGameButton;
    Button loadGameButton;
    Button exitButton;
    VBox startStagePane;
    HBox startLoadArea;
    Scene startScene;
    Stage startStage;

    //Pause Stage
    Button pauseExitButton;
    Button pauseContinueButton;
    Button pauseSaveButton;
    VBox pauseStagePane;
    Scene pauseScene;
    Stage pauseStage;
    HBox pauseSaveGameArea;
    ComboBox startSaveSelecter;
    ComboBox pauseSaveSelecter;


    DataController dataController;
    AutoSafe autoSave;


    private static final Logger log = LogManager.getLogger(Main.class);

    public static void main(String[] args) throws InterruptedException {
        log.info("Started application");
        launch(args);


    }

    public Player getP() {
        return this.p;
    }
    public void newGame() {

         /*
        #####COMMENT########
        For a new Game the player stats a set to low level.
        Further the players booleans are set on false.
        The World is filled with the default map (only Grass)
         */

        p.setX(200);
        p.setY(200);
        p.getInvItem(2).setAmount(10);
        p.getInvItem(3).setAmount(10);
        p.getCurrencyItem().setAmount(20);
        p.setSpeed(p.getDefaultSpeed());
        p.setOnVehicle(false);
        p.setHasTractor(false);
        p.setHasHarvester(false);
        p.setHasATV(true);
        w.fillWorld(w.getDefaultGroundMap());
        w.fillPlanted(w.getDefaultPlantedMap(), p);
        layerPlayer.getChildren().clear();
        layerPlayer.getChildren().add(farmer);

         /*
        #####COMMENT########
        vehicleManager is reseted.
         */


        vehicleManager.getVehicleCollection()[0] = new Tractor();
        vehicleManager.getVehicleCollection()[1] = new Harvester();

        vehicleFactory.createVehicle("ATV", 200, 200);

        //GUI shows the actual Player stats
        mainController.refreshItemAmount();
        farmer.relocate(p.getX(), p.getY());

        //loop fills the GridPane with the correct Images
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid.length; j++) {
                mainController.createImgView(i, j, layerGround, grid);
            }
        }

        for (int i = 0; i < gridPlanted.length; i++) {
            for (int j = 0; j < gridPlanted.length; j++) {
                mainController.createImgViewForCrops(i, j, layerCrop, gridPlanted);
            }
        }

        //Animation timer starts
        timer.start();
        log.info("NewGame Mehtod called.");


    }

    public void loadGame(String game) {

        /*
        #####COMMENT########
        The Load GAme Method needs the Information wich game to load.
        This information is given to the dataController wich sets als relevant Stats in world, vehicle and player.
         */
        dataController.load(game);
        mainController.refreshItemAmount();
        p.setSpeed(p.getDefaultSpeed());

        //After laodinga game player is never on vehicle. --> Too many bugs occured
        p.setOnVehicle(false);
        layerPlayer.getChildren().clear();
        layerPlayer.getChildren().add(farmer);
        farmer.relocate(p.getX(), p.getY());

        //For every vehicle wich is already baucght, the Factory is called.
        if (p.getHasTractor()) {
            vehicleFactory.createVehicle("Tractor", vehicleManager.getVehicleCollection()[0].getX(), vehicleManager.getVehicleCollection()[0].getY());
            log.info("load Game - create Tractor called");
        }

        if (p.getHasHarvester()) {
            vehicleFactory.createVehicle("Harvester", vehicleManager.getVehicleCollection()[1].getX(), vehicleManager.getVehicleCollection()[1].getY());
            log.info("load Game - create Harvester called");
        }

        if (p.getHasATV()) {
            vehicleFactory.createVehicle("ATV", vehicleManager.getVehicleCollection()[2].getX(), vehicleManager.getVehicleCollection()[2].getY());
            log.info("load Game - create ATV called");
        }


        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid.length; j++) {
                mainController.createImgView(i, j, layerGround, grid);
            }
        }

        for (int i = 0; i < gridPlanted.length; i++) {
            for (int j = 0; j < gridPlanted.length; j++) {
                mainController.createImgViewForCrops(i, j, layerCrop, gridPlanted);
                if (gridPlanted[i][j].getIndex() == 8) {
                    mainController.refreshLayer(i, j, p, layerGround, grid, layerCrop, gridPlanted);
                }
            }
        }
        log.info("LoadGame Mehtod called.");



        timer.start();

    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        /*#####COMMENT########
        Main Methode:
         In this Method alle JavaFX elements are initialized. Further: important classes such as player, world or vehicleManager are initialized
         */


        //Start Stage
        /*
        #####COMMENT########
        The following 100 rows declaring the start and pause menu. Start menu appears when main method is started.
        Pause menu appears when ESCAPE is pressed while ingame.
         */
        newGameButton = new Button("New Game");
        newGameButton.setMaxWidth(Double.MAX_VALUE);
        newGameButton.setPadding(new Insets(5, 5, 5, 5));
        newGameButton.setOnAction(e -> {

            //When user hits the NewGamButton a new Game is initalized.
            newGame();

            //Auto Back-Up started
            autoSave.startBackUp();
            primaryStage.show();
            startStage.close();

        });

        //Combobox with Save Options.
        startSaveSelecter = new ComboBox<String>();
        startSaveSelecter.getItems().addAll("Game 1", "Game 2", "Game 3", "Back Up");
        startSaveSelecter.getSelectionModel().selectFirst();


        loadGameButton = new Button("Load Game");
        loadGameButton.setMaxWidth(Double.MAX_VALUE);
        loadGameButton.setPadding(new Insets(5, 5, 5, 5));
        loadGameButton.setOnAction(e -> {

            autoSave.startBackUp();
            log.info(startSaveSelecter.getValue());

            try {
                loadGame(String.valueOf(startSaveSelecter.getValue()));
            } catch (NullPointerException npe) {
                log.info("Nullpointer Exception. No saved Game found. New Game started");
                newGame();
            }

            primaryStage.show();
            startStage.close();
        });
        startLoadArea = new HBox(startSaveSelecter, loadGameButton);
        startLoadArea.setMaxWidth(Double.MAX_VALUE);
        startLoadArea.setSpacing(10);

        exitButton = new Button("Exit");
        exitButton.setMaxWidth(Double.MAX_VALUE);
        exitButton.setPadding(new Insets(5, 5, 5, 5));
        exitButton.setOnAction(e -> System.exit(0));

        startStagePane = new VBox(newGameButton, startLoadArea, exitButton);
        startStagePane.setPadding(new Insets(20, 20, 20, 20));
        startStagePane.setSpacing(15);
        startScene = new Scene(startStagePane, 300, 400);
        startStage = new Stage();
        startStage.setScene(startScene);
        startStage.setTitle("Menu");
        startStage.show();


        //Pause Stage
        pauseExitButton = new Button("Exit");
        pauseExitButton.setMaxWidth(Double.MAX_VALUE);
        pauseExitButton.setPadding(new Insets(5, 5, 5, 5));

        //When exit the current Game, the player must leave the vehcile when on a vehicle.
        pauseExitButton.setOnAction(e -> {
            if(p.getOnVehicle()) {
                vehicleManager.sattleOn(p);
            }
            timer.stop();
            primaryStage.close();
            pauseStage.close();
            startStage.show();

        });

        pauseContinueButton = new Button("Continue Game");
        pauseContinueButton.setMaxWidth(Double.MAX_VALUE);
        pauseContinueButton.setPadding(new Insets(5, 5, 5, 5));
        pauseContinueButton.setOnAction(e -> {
            pauseStage.close();
            timer.start();
        });

        pauseSaveSelecter = new ComboBox<String>();
        pauseSaveSelecter.getItems().addAll("Game 1", "Game 2", "Game 3");
        pauseSaveSelecter.getSelectionModel().selectFirst();

        pauseSaveButton = new Button("Save Game");
        pauseSaveButton.setMaxWidth(Double.MAX_VALUE);
        pauseSaveButton.setPadding(new Insets(5, 5, 5, 5));
        pauseSaveButton.setOnAction(e -> {
            dataController.save(String.valueOf(pauseSaveSelecter.getValue()));
        });

        //Panes for the Menus
        pauseSaveGameArea = new HBox(pauseSaveSelecter, pauseSaveButton);
        pauseSaveGameArea.setMaxWidth(Double.MAX_VALUE);
        pauseSaveGameArea.setSpacing(10);

        pauseStagePane = new VBox(pauseContinueButton, pauseSaveGameArea, pauseExitButton);
        pauseStagePane.setPadding(new Insets(20, 20, 20, 20));
        pauseStagePane.setSpacing(15);

        pauseScene = new Scene(pauseStagePane, 300, 400);
        pauseStage = new Stage();
        pauseStage.setTitle("Pause Menu");
        pauseStage.setScene(pauseScene);


        //Welt erstellen und dazugehöriges GripPane
        primaryStage.setTitle("Farming Simulator");
        w = new World(20, 20);


        grid = w.getGrid();
        gridPlanted = w.getGridPlanted();
        layerCrop = new GridPane();
        layerGround = new GridPane();

        //In Main Method important Classes are initalized. Also associated JavaFx elements.
        p = new Player();
        vehicleManager = new VehicleManager();


        p.invSetActive(0);
        images = new Images();
        imageMap = Images.getImageMap();

        s = new Shop(700, 100, p, vehicleManager);


        //Farmer Image deklarieren und dazugehöriges Pane
        ImageView playerImageView = new ImageView();
        playerImageView.setImage(imageMap.get(p.getImg()));

        tractorImageView = new ImageView();

        harvesterImageView = new ImageView();

        atvImageView = new ImageView();


        ImageView shopImageView = new ImageView();
        shopImageView.setImage(imageMap.get(s.getImg()));

        farmer = new Group(playerImageView);

        layerPlayer = new Pane(farmer);
        vehicleFactory = new VehicleFactory(layerPlayer, vehicleManager, imageMap, atvImageView, harvesterImageView, tractorImageView);

        //Controller initalized
        mainController = new MainController(imageMap, p);
        shopController = new ShopController(imageMap, p, s, vehicleFactory);
        dataController = new DataController(p, w, vehicleManager);
        autoSave = new AutoSafe(dataController);

        shop = new Group(shopImageView);
        shop.relocate(w.calculateCoordinates(14), w.calculateCoordinates(2));
        layerShop = new Pane(shop);


        //Inventar, das dazogehörige Pane und Bilder
        overlay = new BorderPane();
        //coin and currencyAmount
        ImageView coinView = new ImageView(imageMap.get("Coin"));

        ImageView shopCoinView = new ImageView(imageMap.get("Coin"));


        ImageView wheatView = new ImageView(imageMap.get("WheatIcon"));

        ImageView potatoView = new ImageView(imageMap.get("PotatoIcon"));


        //InvItems

        ImageView InvView1 = new ImageView(imageMap.get(p.getInvItem(0).getImg()));
        ImageView InvView2 = new ImageView(imageMap.get(p.getInvItem(1).getImg()));
        ImageView InvView3 = new ImageView(imageMap.get(p.getInvItem(2).getImg()));
        ImageView InvView4 = new ImageView(imageMap.get(p.getInvItem(3).getImg()));


        //inv gui
        /*
        #####COMMENT########
        The next 50 rows declaring the JavaFX elements for the GUI responsing the items
         */
        invItems = new HBox(InvView1, InvView2, InvView3, InvView4);
        invItems.setAlignment(Pos.CENTER);
        invItems.setPadding(new Insets(10));
        invItems.setSpacing(20);

        invBackground = new ImageView();
        invBackground.setImage(imageMap.get("InvActive1"));
        inv = new StackPane(invBackground, invItems);

        currency = new HBox(coinView, mainController.currencyAmount);
        currency.setAlignment(Pos.CENTER_RIGHT);
        currency.setPadding(new Insets(10));
        currency.setSpacing(10);

        wheat = new HBox(wheatView, mainController.wheatAmount);
        wheat.setAlignment(Pos.CENTER_RIGHT);
        wheat.setPadding(new Insets(10));
        wheat.setSpacing(10);

        potato = new HBox(potatoView, mainController.potatoAmount);
        potato.setAlignment((Pos.CENTER_RIGHT));
        potato.setPadding(new Insets(10));
        potato.setSpacing(10);

        resources = new VBox(currency, wheat, potato);
        resources.setSpacing(10);

        overlay.setTop(resources);
        overlay.setBottom(inv);
        overlay.setPadding(new Insets(15));

        GameWorld = new StackPane(layerGround, layerCrop, layerPlayer, layerShop);


        //shop Gui
        /*
        #####COMMENT########
        The following 100 rows declaring JvaFx Elments according to the Shop and Shop Menu
         */

        shopCurrency = new HBox(shopCoinView, shopController.currencyAmountLabel);
        shopCurrency.setAlignment(Pos.CENTER_RIGHT);
        shopCurrency.setPadding(new Insets(10));
        shopCurrency.setSpacing(10);


        shopHeader = new BorderPane();
        shopHeader.setLeft(shopController.shopTitle);
        shopHeader.setRight(shopCurrency);

        //Wheat Trade Buttons and functions
        shopImgWheat = new ImageView(imageMap.get("WheatIconInv"));

        shopController.sellWheatButton.setOnAction(e -> {
            shopController.sellWheatButtonListener();
        });

        shopController.buyWheatButton.setOnAction(e -> {
            shopController.buyWheatButtonListener();
        });

        shopWheatButtons = new HBox(shopController.sellWheatButton, shopImgWheat, shopController.buyWheatButton);
        shopWheatButtons.setAlignment(Pos.CENTER);
        shopWheat = new VBox(shopController.wheatAmountLabel, shopWheatButtons);
        shopWheat.setAlignment(Pos.CENTER);


        //Potato Trade Buttons and functions
        shopImgPotato = new ImageView(imageMap.get("PotatoIconInv"));

        shopController.sellPotatoButton.setOnAction(e -> {
            shopController.sellPotatoButtonListener();
        });

        shopController.buyPotatoButton.setOnAction(e -> {
            shopController.buyPotatoButtonListener();
        });

        shopPotatoButtons = new HBox(shopController.sellPotatoButton, shopImgPotato, shopController.buyPotatoButton);
        shopPotatoButtons.setSpacing(5);
        shopPotatoButtons.setAlignment(Pos.CENTER);
        shopPotato = new VBox(shopController.potatoAmountLabel, shopPotatoButtons);
        shopPotato.setAlignment(Pos.CENTER);


        //Plow Trade Buttons and functions
        shopImgPlow = new ImageView(imageMap.get("Harvester"));

        shopController.sellHarvesterButton.setOnAction(e -> {
            shopController.sellPlowButtonListener();
        });

        shopController.buyHarvesterButton.setOnAction(e -> {
            shopController.buyPlowButtonListener();
        });

        shopPlowButtons = new HBox(shopController.sellHarvesterButton, shopImgPlow, shopController.buyHarvesterButton);
        shopPlowButtons.setAlignment(Pos.CENTER);
        shopPlow = new VBox(shopController.harvesterAmountLabel, shopPlowButtons);
        shopPlow.setAlignment(Pos.CENTER);


        //Hand Trade Buttons and functions
        shopImgHand = new ImageView(imageMap.get("Tractor"));

        shopController.sellTractorButton.setOnAction(e -> {
            shopController.sellTractorButtonListener();
        });

        shopController.buyTractorButton.setOnAction(e -> {
            shopController.buyTractorButtonListener();
        });

        shopHandButtons = new HBox(shopController.sellTractorButton, shopImgHand, shopController.buyTractorButton);
        shopHandButtons.setSpacing(5);
        shopHandButtons.setAlignment(Pos.CENTER);
        shopHand = new VBox(shopController.tractorAmountLabel, shopHandButtons);
        shopHand.setAlignment(Pos.CENTER);

        //Sell Buttonlisteners
        shopController.sellAllButton.setOnAction(e -> {
            shopController.sellAllButtonListener();
            mainController.refreshItemAmount();
        });


        shopController.sellSelectedButton.setOnAction(e -> {
            shopController.setSellSelectedButtonListener();
            mainController.refreshItemAmount();
        });

        shopController.sellSelectedButtonvShop.setOnAction(e -> {
            shopController.setSellSelectedButtonvShopListener();
            mainController.refreshItemAmount();
        });

        //Shopdialog Layout

        shopButtons1 = new HBox(shopWheat, shopPotato);
        shopButtons1.setAlignment(Pos.CENTER);
        shopButtons1.setSpacing(50);


        shopSellButtons = new HBox(shopController.cShop, shopController.vShop, shopController.sellAllButton, shopController.sellSelectedButton);
        shopSellButtons.setAlignment(Pos.CENTER);
        shopSellButtons.setSpacing(30);

        shopController.cShop.setOnAction(e -> {
            shopController.cShopButtonListener();
            shopButtons1.getChildren().clear();
            shopButtons1.getChildren().addAll(shopWheat, shopPotato);
            shopSellButtons.getChildren().clear();
            shopSellButtons.getChildren().addAll(shopController.cShop, shopController.vShop, shopController.sellAllButton, shopController.sellSelectedButton);
        });

        shopController.vShop.setOnAction(e -> {
            shopController.vShopButtonListener();
            shopButtons1.getChildren().clear();
            shopButtons1.getChildren().addAll(shopPlow, shopHand);
            shopSellButtons.getChildren().clear();
            shopSellButtons.getChildren().addAll(shopController.cShop, shopController.vShop, shopController.sellSelectedButtonvShop);
        });

        shopLayout = new VBox(shopHeader, shopButtons1, shopSellButtons);


        /*
        #####COMMENT########
        At the end the Scenes and stages are build together.
         */
        rootPane = new StackPane();
        rootPane.getChildren().addAll(GameWorld, overlay);
        mainScene = new Scene(rootPane);
        primaryStage.setScene(mainScene);


        shopPane = new StackPane(shopLayout);
        shopScene = new Scene(shopPane);
        shopStage = new Stage();
        shopStage.setScene(shopScene);
        shopStage.setMinHeight(300);
        shopStage.setMinWidth(500);


        //Methode um per W A S D den bauern zu steuern
        /*
        #####COMMENT########
        Every Key Handling happens in the mainController. The listener needs many diffrent JavaFX-elements to walk with. It grew over the time
        In the future we will just put the wohle Main-Class into it and work with getter and setter
         */
        mainScene.setOnKeyPressed(e -> mainController.keyPressedListener(e.getCode(), p, s, w, shopStage, layerGround, grid, layerCrop, gridPlanted, invBackground, vehicleManager, pauseStage));
        mainScene.setOnKeyReleased(e -> mainController.keyRelease(e.getCode(), vehicleManager));

        /*
        #####COMMENT########
        The animationTimer GameLoop to handle our movement.
        While you press the movement Keys(W A S D) a specific boolean is set on true. On key release the boolean is false
        Every sensible Combination is interpreted by the AnimationTimer and for every tick the player coordinates are setted in a certain direction.
        The actual image position is refreshed at the end of th timer.
        When the player is on a vehicle, the active vehicle follows the player movement.
         */
        timer = new AnimationTimer() {
            @Override
            public void handle(long l) {
                if (p.getY() <= 0) {
                    mainController.wPressed.set(false);
                    log.info("can't move out of bounds");
                }
                if (mainController.wPressed.get() && !mainController.aPressed.get() && !mainController.sPressed.get() && !mainController.dPressed.get()) {
                    p.setY(p.getYAsDouble() - p.getSpeed());
                    farmer.relocate(p.getXAsDouble(), p.getYAsDouble());
                    if (p.getOnVehicle()) {

                        vehicleManager.getActiveVehicle().setY(p.getYAsDouble() - p.getSpeed());
                        vehicleManager.getActiveVehicleGroup().relocate(p.getXAsDouble(), p.getYAsDouble());
                    }
                }

                if (p.getX() <= 0) {
                    mainController.aPressed.set(false);
                    log.info("can't move out of bounds");
                }
                if (!mainController.wPressed.get() && mainController.aPressed.get() && !mainController.sPressed.get() && !mainController.dPressed.get()) {
                    p.setX(p.getXAsDouble() - p.getSpeed());
                    farmer.relocate(p.getXAsDouble(), p.getYAsDouble());
                    if (p.getOnVehicle()) {
                        vehicleManager.getActiveVehicle().setX(p.getXAsDouble() - p.getSpeed());
                        vehicleManager.getActiveVehicleGroup().relocate(p.getXAsDouble(), p.getYAsDouble());
                    }
                }

                if (p.getX() >= 951) {
                    mainController.dPressed.set(false);
                    log.info("can't move out of bounds");
                }
                if (!mainController.wPressed.get() && !mainController.aPressed.get() && !mainController.sPressed.get() && mainController.dPressed.get()) {
                    p.setX(p.getXAsDouble() + p.getSpeed());
                    farmer.relocate(p.getXAsDouble(), p.getYAsDouble());
                    if (p.getOnVehicle()) {
                        vehicleManager.getActiveVehicle().setX(p.getXAsDouble() + p.getSpeed());
                        vehicleManager.getActiveVehicleGroup().relocate(p.getXAsDouble(), p.getYAsDouble());
                    }
                }

                if (p.getY() >= 951) {
                    mainController.sPressed.set(false);
                    log.info("can't move out of bounds");
                }
                if (!mainController.wPressed.get() && !mainController.aPressed.get() && mainController.sPressed.get() && !mainController.dPressed.get()) {
                    p.setY(p.getYAsDouble() + p.getSpeed());
                    farmer.relocate(p.getXAsDouble(), p.getYAsDouble());
                    if (p.getOnVehicle()) {
                        vehicleManager.getActiveVehicle().setY(p.getYAsDouble() + p.getSpeed());
                        vehicleManager.getActiveVehicleGroup().relocate(p.getXAsDouble(), p.getYAsDouble());
                    }
                }

                if (mainController.wPressed.get() && !mainController.aPressed.get() && !mainController.sPressed.get() && mainController.dPressed.get()) {
                    p.setX(p.getXAsDouble() + (p.getSpeed() * sin(45)));
                    p.setY(p.getYAsDouble() - (p.getSpeed() * sin(45)));
                    farmer.relocate(p.getXAsDouble(), p.getYAsDouble());
                    if (p.getOnVehicle()) {
                        vehicleManager.getActiveVehicle().setX(p.getXAsDouble() + (p.getSpeed() * sin(45)));
                        vehicleManager.getActiveVehicle().setY(p.getYAsDouble() - (p.getSpeed() * sin(45)));
                        vehicleManager.getActiveVehicleGroup().relocate(p.getXAsDouble(), p.getYAsDouble());
                    }
                }

                if (mainController.wPressed.get() && mainController.aPressed.get() && !mainController.sPressed.get() && !mainController.dPressed.get()) {
                    p.setX(p.getXAsDouble() - (p.getSpeed() * sin(45)));
                    p.setY(p.getYAsDouble() - (p.getSpeed() * sin(45)));
                    farmer.relocate(p.getXAsDouble(), p.getYAsDouble());
                    if (p.getOnVehicle()) {
                        vehicleManager.getActiveVehicle().setX(p.getXAsDouble() - (p.getSpeed() * sin(45)));
                        vehicleManager.getActiveVehicle().setY(p.getYAsDouble() - (p.getSpeed() * sin(45)));
                        vehicleManager.getActiveVehicleGroup().relocate(p.getXAsDouble(), p.getYAsDouble());
                    }
                }

                if (!mainController.wPressed.get() && !mainController.aPressed.get() && mainController.sPressed.get() && mainController.dPressed.get()) {
                    p.setX(p.getXAsDouble() + (p.getSpeed() * sin(45)));
                    p.setY(p.getYAsDouble() + (p.getSpeed() * sin(45)));
                    farmer.relocate(p.getXAsDouble(), p.getYAsDouble());
                    if (p.getOnVehicle()) {
                        vehicleManager.getActiveVehicle().setX(p.getXAsDouble() + (p.getSpeed() * sin(45)));
                        vehicleManager.getActiveVehicle().setY(p.getYAsDouble() + (p.getSpeed() * sin(45)));
                        vehicleManager.getActiveVehicleGroup().relocate(p.getXAsDouble(), p.getYAsDouble());
                    }
                }

                if (!mainController.wPressed.get() && mainController.aPressed.get() && mainController.sPressed.get() && !mainController.dPressed.get()) {
                    p.setX(p.getXAsDouble() - (p.getSpeed() * sin(45)));
                    p.setY(p.getYAsDouble() + (p.getSpeed() * sin(45)));
                    farmer.relocate(p.getXAsDouble(), p.getYAsDouble());
                    if (p.getOnVehicle()) {
                        vehicleManager.getActiveVehicle().setX(p.getXAsDouble() - (p.getSpeed() * sin(45)));
                        vehicleManager.getActiveVehicle().setY(p.getYAsDouble() + (p.getSpeed() * sin(45)));
                        vehicleManager.getActiveVehicleGroup().relocate(p.getXAsDouble(), p.getYAsDouble());
                    }
                }

                if (mainController.refreshPlayer.get()) {
                    farmer.relocate(p.getXAsDouble(), p.getYAsDouble());
                }
            }
        };

    }
}
