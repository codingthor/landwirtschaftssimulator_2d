package org;
import java.util.*;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.vehicles.VehicleManager;

public class Shop extends Building{
    private static final Logger log = LogManager.getLogger(Shop.class);
    ShopManager shopManager;

    private HashMap<String, Offer> offerMap;

    Shop(int x, int y, Player player, VehicleManager vm){
        log.info("created Shop");
        setX(x);
        setY(y);
        this.shopManager = new ShopManager(player, vm);
        offerMap = shopManager.getOfferMap();
        setImg("Shop");
    }

    public void sellAll(){
        shopManager.tradeLoop(offerMap.get("SellWheat"), shopManager.getPlayer().getWheatItem().getAmount());
        shopManager.tradeLoop(offerMap.get("SellPotato"), shopManager.getPlayer().getPotatoItem().getAmount());
    }

    public void shopSystem(int amountWheat, int amountPotato, int amountPlow, int amountHand){
        if (amountWheat>=0){
            shopManager.tradeLoop(offerMap.get("BuyWheat"), amountWheat);
        } else {
            shopManager.tradeLoop(offerMap.get("SellWheat"), amountWheat*(-1));
        }
        if (amountPotato>=0){
            shopManager.tradeLoop(offerMap.get("BuyPotato"), amountPotato);
        } else {
            shopManager.tradeLoop(offerMap.get("SellPotato"), amountPotato*(-1));
        }
        if (amountPlow==1){
            offerMap.get("BuyPlow").trade();
        }
        if (amountHand==1){
            offerMap.get("BuyHand").trade();
        }
    }

    public boolean isShopNear(int playerX, int playerY){
        int shopX = calculateCoordinates(this.getX());
        int shopY = calculateCoordinates(this.getY());
        return Stream.of(shopX, shopX+1).anyMatch(i -> i == playerX) && Stream.of(shopY, shopY+1).anyMatch(i -> i == playerY);
    }
}