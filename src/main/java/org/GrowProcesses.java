package org;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.items.Item;
import java.util.Timer;
import java.util.TimerTask;



public class GrowProcesses implements Runnable{
    private static final Logger log = LogManager.getLogger(GrowProcesses.class);

    private final World world;
    private final Item item;
    private final int x;
    private final int y;

    private final Timer timer;

    private final TimerTask task;

    public GrowProcesses(World w, int PlayerX, int PlayerY, Item item){
        log.debug("created GrowProcesses");
        this.world = w;
        this.x = PlayerX;
        this.y = PlayerY;
        this.item = item;
        timer = new Timer();
        task = new TimerTask() {
            @Override
            public void run() {

                world.changeToPlantedGroundReady(x, y, item);
                log.debug("run exit");

            }
        };
        timer.schedule(task, 3000);
    }

    @Override
    public void run() {
        log.debug("run called");
        world.changeToPlantedGround(x, y, item);


    }
}
