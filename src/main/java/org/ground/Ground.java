package org.ground;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class Ground {
    private static final Logger log = LogManager.getLogger(Ground.class);

    private int index;

    private double speedFactor;
    private String imagePath;

    public Ground() {
        log.debug("created Ground");
        setIndex();
    }

    public int getIndex() {
        log.debug("getIndex called: " + this.index);
        return index;
    }

    public void setIndex(int index) {
        log.debug("setIndex(int) called");
        this.index = index;
        log.debug("setIndex to: " + this.index);
    }

    public void setIndex() {
        log.debug("setIndex called");
        this.index = 0;
        log.debug("setIndex to: " + this.index);
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}


