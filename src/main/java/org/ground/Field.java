package org.ground;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Field extends Ground {
    private static final Logger log = LogManager.getLogger(Field.class);



    public Field(){
        log.info("created Field");
        setIndex();
        setImagePath("Field");
    }

    @Override
    public void setIndex() {
        log.info("setIndex called");
        setIndex(2);
    }

    @Override
    public void setImagePath(String imagePath) {
        super.setImagePath(imagePath);
    }

}
