package org.ground.crops;

import org.ground.Ground;
import org.items.Item;

public class PlantedGroundReady extends Ground {
    Item item;

    public PlantedGroundReady(Item item) {
        this.item = item;
        setImagePath(this.item.getImg());
        setIndex(81);
    }

    public Item getItem() {
        return this.item;
    }

    @Override
    public String getImagePath() {
        String itemType = item.toString();
        String imgPath = "";
        switch (itemType) {
            case "Potato":
                imgPath = "PotatoReady";
                break;
            case "Wheat":
                imgPath = "WheatReady";
                break;
        }
        return imgPath;
    }
}
