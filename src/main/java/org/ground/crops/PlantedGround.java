package org.ground.crops;

import org.ground.Ground;
import org.items.Item;

public class PlantedGround extends Ground {
    Item item;
    public PlantedGround(Item item) {
        this.item = item;
        setImagePath(this.item.getImg());
        setIndex(8);
    }
    public Item getItem(){
        return this.item;
    }

    @Override
    public String getImagePath() {
        String itemType = item.toString();
        String imgPath = "";
        switch (itemType){
            case "Potato":
                imgPath = "PotatoGrowing";
                break;
            case "Wheat":
                imgPath = "WheatGrowing";
                break;
        }
        return imgPath;
    }
}
