package org.ground;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Grass extends Ground {



    private static final Logger log = LogManager.getLogger(Grass.class);

    public Grass(){
        log.debug("created Grass");
        setIndex();
        setImagePath("Grass");


    }

    @Override
    public void setIndex() {
        log.debug("setIndex called");
        setIndex(1);
    }

    @Override
    public void setImagePath(String imagePath) {
        super.setImagePath(imagePath);
    }
}
