package org;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.items.Item;

public class Offer {
    Item itemToGive;
    Item itemToGet;
    int itemToGiveAmount;
    int itemToGetAmount;
    private static final Logger log = LogManager.getLogger(Offer.class);
    Offer(Item itemToGet, int itemToGetAmount, Item itemToGive, int itemToGiveAmount){
        log.debug("created Offer()");
        this.itemToGet = itemToGet;
        this.itemToGetAmount = itemToGetAmount;
        this.itemToGive = itemToGive;
        this.itemToGiveAmount = itemToGiveAmount;
    }


    public void trade(){
        log.debug("used trade()");
        if (itemToGive.getAmount()-itemToGiveAmount>=0){
            itemToGet.setAmount(itemToGet.getAmount()+itemToGetAmount);
            itemToGive.setAmount(itemToGive.getAmount()-itemToGiveAmount);
        } else {
            log.info("You cant do this trade!");
        }
    }
}
