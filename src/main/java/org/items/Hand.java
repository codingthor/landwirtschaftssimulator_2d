package org.items;

import org.ground.Ground;
import org.ground.crops.PlantedGroundReady;
import org.Player;
import org.World;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Hand extends Item{
    private static final Logger log = LogManager.getLogger(Hand.class);
    public Hand(int amount) {
        super(amount);
        log.debug("created Hand");
        setImg("HandIcon");
        setActive(false);
    }

    @Override
    public void use(World world, Player player) {
        log.debug("used use()");
        int playerX = player.calculateCoordinates(player.getX());
        int playerY = player.calculateCoordinates(player.getY());
        int gridType = world.getGridType(playerX, playerY);
        int gridPlantedType = world.getGridPlantedType(playerX, playerY);
        if (gridType==2 && gridPlantedType==81){
            Ground[][] gridPlanted = world.getGridPlanted();
            PlantedGroundReady p = (PlantedGroundReady) gridPlanted[playerX][playerY];
            world.changeToEmpty(playerX, playerY);
            world.changeToGrass(playerX, playerY);
            p.getItem().setAmount(p.getItem().getAmount()+2);
        } else {
            log.debug("cant use use() here");
        }
    }
}
