package org.items;

import org.Player;
import org.World;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Item {
    private static final Logger log = LogManager.getLogger(Item.class);
    private int amount;
    private String img;
    private boolean active;

    public Item(int amount){
        this.amount = amount;
        this.active = false;
    }


    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void use(World world, Player player){
    }
}
