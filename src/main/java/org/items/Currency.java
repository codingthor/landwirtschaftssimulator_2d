package org.items;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Currency extends Item{
    private static final Logger log = LogManager.getLogger(Currency.class);
    public Currency(int amount) {
        super(amount);
        log.debug("created Currency");
        this.setImg("Coin");
        this.setActive(false);
    }
}