package org.items;

import org.GrowProcesses;
import org.Player;
import org.World;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Crop extends Item{

    private static final Logger log = LogManager.getLogger(Crop.class);
    Crop(int amount) {
        super(amount);
        log.debug("created Crop");
        setImg("");
        setActive(false);
    }

    @Override
    public void use(World world, Player player) {
        log.debug("used use()");
        int playerX = player.calculateCoordinates(player.getX());
        int playerY = player.calculateCoordinates(player.getY());
        int gridType = world.getGridType(playerX, playerY);
        int gridPlantedType = world.getGridPlantedType(playerX, playerY);
        if (gridType == 2 && this.getAmount()>=1 && gridPlantedType == 404){
            GrowProcesses growProcesses = new GrowProcesses(world,playerX,playerY,this);
            growProcesses.run();
            this.setAmount(this.getAmount()-1);
        } else {
            log.info("You cant saw here or you dont have enough crops!");
        }
    }


}
