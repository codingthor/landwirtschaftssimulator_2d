package org.items;

import org.Player;
import org.World;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Plow extends Item{
    private static final Logger log = LogManager.getLogger(Plow.class);
    public Plow(int amount){
        super(amount);
        log.debug("created Plow");
        this.setImg("PlowIcon");
        this.setActive(false);
    }

    @Override
    public void use(World world, Player player) {
        log.debug("used use()");
        int playerX = player.calculateCoordinates(player.getX());
        int playerY = player.calculateCoordinates(player.getY());
        int gridType = world.getGridType(playerX, playerY);
        if (gridType==1){
            world.changeToField(playerX, playerY);
        } else {
            log.debug("Cant plow here");
        }
    }
}
