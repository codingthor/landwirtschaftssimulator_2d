package org.items;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Wheat extends Crop{
    private static final Logger log = LogManager.getLogger(Wheat.class);
    public Wheat(int amount) {
        super(amount);
        log.debug("created Wheat");
        this.setImg("WheatIconInv");
        this.setActive(false);
    }

    @Override
    public String toString() {
        return "Wheat";
    }


}