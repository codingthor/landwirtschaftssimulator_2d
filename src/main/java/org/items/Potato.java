package org.items;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Potato extends Crop{
    private static final Logger log = LogManager.getLogger(Potato.class);
    public Potato(int amount) {
        super(amount);
        log.debug("created Potato");
        this.setImg("PotatoIconInv");
        this.setActive(false);

    }

    @Override
    public String toString() {
        return "Potato";
    }


}
