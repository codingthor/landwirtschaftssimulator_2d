package org;


import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;

import java.util.Timer;
import java.util.TimerTask;

import java.util.HashMap;

import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ground.Ground;
import org.vehicles.VehicleManager;


public class MainController {

    BooleanProperty wPressed = new SimpleBooleanProperty(false);
    BooleanProperty aPressed = new SimpleBooleanProperty(false);
    BooleanProperty dPressed = new SimpleBooleanProperty(false);
    BooleanProperty sPressed = new SimpleBooleanProperty(false);
    BooleanProperty refreshPlayer = new SimpleBooleanProperty(false);
    Label currencyAmount;
    Label wheatAmount;
    Label potatoAmount;
    HashMap<String, Image> imageMap;

    Player p;



    Timer timer = new Timer();





    public MainController(HashMap<String, Image> imageMap, Player p){
        this.p = p;
        this.imageMap = imageMap;
        this.currencyAmount = new Label("" + p.getCurrencyItem().getAmount());
        this.currencyAmount.setStyle("-fx-font-size: 30");

        this.wheatAmount = new Label("" + p.getWheatItem().getAmount());
        this.wheatAmount.setStyle("-fx-font-size: 30");

        this.potatoAmount = new Label("" + p.getPotatoItem().getAmount());
        this.potatoAmount.setStyle("-fx-font-size: 30");
    }


    private static final Logger log = LogManager.getLogger(MainController.class);

//The most important main controller method
    public void keyPressedListener(KeyCode e, Player p, Shop s, World w, Stage shopStage, GridPane layerGround, Ground[][] grid, GridPane layerPlanted, Ground[][] gridPlanted, ImageView invBG, VehicleManager vehicleManager, Stage pauseStage){

        /*
        #####COMMENT########
        Following 30 rows to set the movement booleans wicha re interpreted in Aniamtion timer
         */
        if (e == KeyCode.W) {
            wPressed.set(true);
            log.debug("W-Key pressed");
            if(p.getOnVehicle()){
                vehicleManager.getActiveVehicle().getDriveVertical().set(true);
            }

        }

        if (e == KeyCode.A) {
            aPressed.set(true);
            log.debug("A-Key pressed");
            if(p.getOnVehicle()){
                vehicleManager.getActiveVehicle().getDriveHorizontal().set(true);

            }

        }


        if (e == KeyCode.S) {
            log.debug("S-Key pressed");
            sPressed.set(true);
            if(p.getOnVehicle()){

                vehicleManager.getActiveVehicle().getDriveVertical().set(true);
            }
        }

        if (e == KeyCode.D) {
            log.debug("D-Key pressed");
            dPressed.set(true);
            if(p.getOnVehicle()){
                vehicleManager.getActiveVehicle().getDriveHorizontal().set(true);

            }
        }

        //Key C to sit on a vehicle or leave a vehicle

        if (e == KeyCode.C) {
            log.debug("C-Key pressed");
            vehicleManager.sattleOn(p);
        }
        //open Pausemenu
        if (e == KeyCode.ESCAPE){
            log.debug("ESC-Key pressed");
            pauseStage.show();
        }
            //When this is true, the Farmer GUI is on shown on the actual position.
            refreshPlayer.set(true);


        /*
        #####COMMENT########
        Space is the Use/interact button. When on Vehicle only the vehicle ability is available
        When not he can open the shop with all items or use the specific item ability.
        The item ability is specified in the item class
         */
        if (e == KeyCode.SPACE) {
            log.debug("SPACE-Key pressed");
            if(!p.getOnVehicle()){


            int pX = p.calculateCoordinates(p.getX());
            int pY = p.calculateCoordinates(p.getY());

            if (s.isShopNear(pX,pY)){
                wPressed.set(false);
                aPressed.set(false);
                sPressed.set(false);
                dPressed.set(false);
                log.info("Opened Shop");
                //Open Shop window here
                shopStage.show();
                return;
            }

            p.invGetActive().use(w, p);


            this.wheatAmount.setText("" + p.getWheatItem().getAmount());
            this.potatoAmount.setText("" + p.getPotatoItem().getAmount());

                createImgView(p.calculateCoordinates(p.getX()),  p.calculateCoordinates(p.getY()), layerGround, grid);
                createImgViewForCrops(p.calculateCoordinates(p.getX()),  p.calculateCoordinates(p.getY()), layerPlanted, gridPlanted);

            if(p.getInvItem(2).isActive() || p.getInvItem(3).isActive()){
                refreshLayer(pX, pY, p, layerGround, grid, layerPlanted, gridPlanted);

            }
        }

            //All possibilities to change the world while on a vehicle are covered
            //
            if(p.getOnVehicle()){
                vehicleManager.getActiveVehicle().vehicleAbility(w, p);
                createImgView(p.calculateCoordinates(p.getX()),  p.calculateCoordinates(p.getY()), layerGround, grid);
                createImgView(p.calculateCoordinates(p.getX())-1,  p.calculateCoordinates(p.getY()), layerGround, grid);
                createImgView(p.calculateCoordinates(p.getX())+1,  p.calculateCoordinates(p.getY()), layerGround, grid);
                createImgView(p.calculateCoordinates(p.getX()),  p.calculateCoordinates(p.getY())-1, layerGround, grid);
                createImgView(p.calculateCoordinates(p.getX()),  p.calculateCoordinates(p.getY())+1, layerGround, grid);

                createImgViewForCrops(p.calculateCoordinates(p.getX()),  p.calculateCoordinates(p.getY()), layerPlanted, gridPlanted);
                createImgViewForCrops(p.calculateCoordinates(p.getX())-1,  p.calculateCoordinates(p.getY()), layerPlanted, gridPlanted);
                createImgViewForCrops(p.calculateCoordinates(p.getX())+1,  p.calculateCoordinates(p.getY()), layerPlanted, gridPlanted);
                createImgViewForCrops(p.calculateCoordinates(p.getX()),  p.calculateCoordinates(p.getY())-1, layerPlanted, gridPlanted);
                createImgViewForCrops(p.calculateCoordinates(p.getX()),  p.calculateCoordinates(p.getY())+1, layerPlanted, gridPlanted);

                if(vehicleManager.getActiveVehicle().getImg()== "Harvester"){
                    this.wheatAmount.setText("" + p.getWheatItem().getAmount());
                    this.potatoAmount.setText("" + p.getPotatoItem().getAmount());
                }

            }
        }

        //Here the active item is setted
        if (e == KeyCode.DIGIT1){
            p.invSetActive(0);
            invBG.setImage(imageMap.get("InvActive1"));
            log.info("Item 1 Active");
        }

        if (e == KeyCode.DIGIT2){
            p.invSetActive(1);
            invBG.setImage(imageMap.get("InvActive2"));
            log.info("Item 2 Active");
        }

        if (e == KeyCode.DIGIT3){
            p.invSetActive(2);
            invBG.setImage(imageMap.get("InvActive3"));
            log.info("Item 3 Active");
        }

        if (e == KeyCode.DIGIT4){
            p.invSetActive(3);
            invBG.setImage(imageMap.get("InvActive4"));
            log.info("Item 4 Active");
        }


    }

    //Key release listener sets all movement booleans to false
    public void keyRelease(KeyCode e, VehicleManager vehicleManager){
        if (e == KeyCode.W) {
            wPressed.set(false);
            if(p.getOnVehicle()){
                vehicleManager.getActiveVehicle().getDriveVertical().set(false);
            }

            log.debug("W-Key released");
        }

        if (e == KeyCode.A) {
            aPressed.set(false);
            if(p.getOnVehicle()){
                vehicleManager.getActiveVehicle().getDriveHorizontal().set(false);
            }
            log.debug("A-Key released");
        }


        if (e == KeyCode.S) {
            sPressed.set(false);
            if(p.getOnVehicle()){
                vehicleManager.getActiveVehicle().getDriveVertical().set(false);
            }
            log.debug("S-Key released");
        }

        if (e == KeyCode.D) {
            dPressed.set(false);
            if(p.getOnVehicle()){
                vehicleManager.getActiveVehicle().getDriveHorizontal().set(false);
            }
            log.debug("D-Key released");
        }

        if (e == KeyCode.C) {
            refreshPlayer.set(false);
            log.debug("C-Key released");
        }

    }

    //this method refreshes they one part of the Ground-GridPane (Grass and Field) and one part of Crop-GridPane (Potatos or Wheat) after a given time period.  Usallay used after harvesting.
    //With this method, the player sees when and the crop is ready to harvest.

    public void refreshLayer(int x, int y,Player p, GridPane groundLayer, Ground[][] grid, GridPane plantedLayer, Ground[][] plantedGrid){
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    createImgView(x, y, groundLayer, grid);
                    createImgViewForCrops(x, y, plantedLayer, plantedGrid);
                });
            }
        }, 3000);

    }


    //This method refreshs one part of the Ground-GridPane (Grass and Field) and one part of Crop-GridPane (Potatos or Wheat) immediately. Usallay used after plowing or harvesting.
    //The method initalize a new ImageView. The image is getted from the world 2d array
    //Then removes the curretn imageView in the GridPane
    //Sets the new ImageView in the GridPane
    public void createImgView(int x, int y, GridPane groundLayer, Ground[][] grid) {
        try {
            ImageView imgView = new ImageView();
            imgView.setImage(imageMap.get(grid[x][y].getImagePath()));
            groundLayer.getChildren().remove(getNode(x, y, groundLayer));
            groundLayer.add(imgView, x, y);
        }
        catch(ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException){
            log.warn("createImgView called for Coordinates out of Game World." + arrayIndexOutOfBoundsException);

        }

    }

    //This method refreshs one part of Crop-GridPane (Potatos or Wheat) immediately. Usallay used after plowing or harvesting.
    //The method initalize a new ImageView. The image is getted from the world 2d array
    //Then removes the curretn imageView in the GridPane
    //Sets the new ImageView in the GridPane
    public void createImgViewForCrops(int x, int y, GridPane plantedLayer, Ground[][] plantedGrid) {
        try {
            ImageView imgView = new ImageView();
            imgView.setImage(imageMap.get(plantedGrid[x][y].getImagePath()));

            plantedLayer.getChildren().remove(getNode(x, y, plantedLayer));

            plantedLayer.add(imgView, x, y);
        }catch(ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException){
            log.warn("createImgViewForCrops called for Coordinates out of Game World." + arrayIndexOutOfBoundsException);

        }

    }
    //Method to get the node on given postition in a gridPane
    public Node getNode(final int x, final int y, GridPane layer) {
        Node result = null;
        ObservableList<Node> c = layer.getChildren();

        for (Node node : c) {
            if(GridPane.getColumnIndex(node) == x && GridPane.getRowIndex(node) == y) {
                result = node;
                break;
            }
        }


        return result;
    }

    public void refreshItemAmount(){
        this.wheatAmount.setText("" + p.getWheatItem().getAmount());
        this.currencyAmount.setText("" + p.getCurrencyItem().getAmount());
        this.potatoAmount.setText("" + p.getPotatoItem().getAmount());
    }

}
