module gui {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.apache.logging.log4j;

    opens org.example to javafx.fxml;
    exports org.example;
    exports org;
    opens org to javafx.fxml;
    exports org.ground;
    opens org.ground to javafx.fxml;
    exports org.ground.crops;
    opens org.ground.crops to javafx.fxml;
}